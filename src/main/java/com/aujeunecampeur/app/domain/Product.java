package com.aujeunecampeur.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "product")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nb_stock", nullable = false)
    private Integer nbStock;

    @NotNull
    @Column(name = "version", nullable = false)
    private Integer version;

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<OrderLine> orderLines = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "products", allowSetters = true)
    private ProductModel productModel;

    @ManyToMany(mappedBy = "products")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Caracteristic> caracteristics = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNbStock() {
        return nbStock;
    }

    public Product nbStock(Integer nbStock) {
        this.nbStock = nbStock;
        return this;
    }

    public void setNbStock(Integer nbStock) {
        this.nbStock = nbStock;
    }

    public Integer getVersion() {
        return version;
    }

    public Product version(Integer version) {
        this.version = version;
        return this;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Set<OrderLine> getOrderLines() {
        return orderLines;
    }

    public Product orderLines(Set<OrderLine> orderLines) {
        this.orderLines = orderLines;
        return this;
    }

    public Product addOrderLine(OrderLine orderLine) {
        this.orderLines.add(orderLine);
        orderLine.setProduct(this);
        return this;
    }

    public Product removeOrderLine(OrderLine orderLine) {
        this.orderLines.remove(orderLine);
        orderLine.setProduct(null);
        return this;
    }

    public void setOrderLines(Set<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public ProductModel getProductModel() {
        return productModel;
    }

    public Product productModel(ProductModel productModel) {
        this.productModel = productModel;
        return this;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public Set<Caracteristic> getCaracteristics() {
        return caracteristics;
    }

    public Product caracteristics(Set<Caracteristic> caracteristics) {
        this.caracteristics = caracteristics;
        return this;
    }

    public Product addCaracteristic(Caracteristic caracteristic) {
        this.caracteristics.add(caracteristic);
        caracteristic.getProducts().add(this);
        return this;
    }

    public Product removeCaracteristic(Caracteristic caracteristic) {
        this.caracteristics.remove(caracteristic);
        caracteristic.getProducts().remove(this);
        return this;
    }

    public void setCaracteristics(Set<Caracteristic> caracteristics) {
        this.caracteristics = caracteristics;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", nbStock=" + getNbStock() +
            ", version=" + getVersion() +
            "}";
    }
}
