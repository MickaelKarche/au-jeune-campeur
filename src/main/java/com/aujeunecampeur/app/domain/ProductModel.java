package com.aujeunecampeur.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProductModel.
 */
@Entity
@Table(name = "product_model")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@org.springframework.data.elasticsearch.annotations.Document(indexName = "productmodel")
public class ProductModel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "label", nullable = false)
    private String label;

    @NotNull
    @Column(name = "brand", nullable = false)
    private String brand;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "price", nullable = false)
    private Double price;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "image_url")
    private String imageUrl;

    @Min(value = 0)
    @Column(name = "nb_selled")
    private Integer nbSelled;

    @OneToMany(mappedBy = "productModel")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Product> products = new HashSet<>();

    @ManyToMany(mappedBy = "productModels")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnore
    private Set<Category> categories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public ProductModel label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getBrand() {
        return brand;
    }

    public ProductModel brand(String brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return price;
    }

    public ProductModel price(Double price) {
        this.price = price;
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public ProductModel description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public ProductModel imageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getNbSelled() {
        return nbSelled;
    }

    public ProductModel nbSelled(Integer nbSelled) {
        this.nbSelled = nbSelled;
        return this;
    }

    public void setNbSelled(Integer nbSelled) {
        this.nbSelled = nbSelled;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public ProductModel products(Set<Product> products) {
        this.products = products;
        return this;
    }

    public ProductModel addProduct(Product product) {
        this.products.add(product);
        product.setProductModel(this);
        return this;
    }

    public ProductModel removeProduct(Product product) {
        this.products.remove(product);
        product.setProductModel(null);
        return this;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public ProductModel categories(Set<Category> categories) {
        this.categories = categories;
        return this;
    }

    public ProductModel addCategory(Category category) {
        this.categories.add(category);
        category.getProductModels().add(this);
        return this;
    }

    public ProductModel removeCategory(Category category) {
        this.categories.remove(category);
        category.getProductModels().remove(this);
        return this;
    }

    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductModel)) {
            return false;
        }
        return id != null && id.equals(((ProductModel) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductModel{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", brand='" + getBrand() + "'" +
            ", price=" + getPrice() +
            ", description='" + getDescription() + "'" +
            ", imageUrl='" + getImageUrl() + "'" +
            ", nbSelled=" + getNbSelled() +
            "}";
    }
}
