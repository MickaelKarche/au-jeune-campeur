package com.aujeunecampeur.app.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    COMPLETED, PAID, SENT, INPROCESS
}
