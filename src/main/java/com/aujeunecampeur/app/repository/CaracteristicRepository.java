package com.aujeunecampeur.app.repository;

import com.aujeunecampeur.app.domain.Caracteristic;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Caracteristic entity.
 */
@Repository
public interface CaracteristicRepository extends JpaRepository<Caracteristic, Long> {

    @Query(value = "select distinct caracteristic from Caracteristic caracteristic left join fetch caracteristic.products",
        countQuery = "select count(distinct caracteristic) from Caracteristic caracteristic")
    Page<Caracteristic> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct caracteristic from Caracteristic caracteristic left join fetch caracteristic.products")
    List<Caracteristic> findAllWithEagerRelationships();

    @Query("select caracteristic from Caracteristic caracteristic left join fetch caracteristic.products where caracteristic.id =:id")
    Optional<Caracteristic> findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select caracteristics from Caracteristic caracteristics left join fetch caracteristics.products p where p.id =:id")
    Optional<Caracteristic[]> findCaracteristicsOfProduct(@Param("id") Long id);
}
