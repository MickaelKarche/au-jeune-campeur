package com.aujeunecampeur.app.repository;

import com.aujeunecampeur.app.domain.Order;

import com.aujeunecampeur.app.domain.User;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Order entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {


}
