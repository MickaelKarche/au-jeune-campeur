package com.aujeunecampeur.app.repository;

import com.aujeunecampeur.app.domain.Product;
import com.aujeunecampeur.app.domain.ProductModel;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the ProductModel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductModelRepository extends JpaRepository<ProductModel, Long> {

}
