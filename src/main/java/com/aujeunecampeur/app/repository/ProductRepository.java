package com.aujeunecampeur.app.repository;

import com.aujeunecampeur.app.domain.Product;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Product entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    //@Query("SELECT c FROM Caracteristic c WHERE c.products.id In (SELECT p FROM Product p, ProductModel pm  WHERE p.productModel.id = pm.id A pm.id = :productModelId)")
    @Query("SELECT p FROM Product p, ProductModel pm  WHERE p.productModel.id = pm.id and pm.id = :productModelId")
    Optional<Product[]> productsOfProductModel(@Param("productModelId") Long productModelId );

}
