package com.aujeunecampeur.app.repository.search;

import com.aujeunecampeur.app.domain.Caracteristic;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Caracteristic} entity.
 */
public interface CaracteristicSearchRepository extends ElasticsearchRepository<Caracteristic, Long> {
}
