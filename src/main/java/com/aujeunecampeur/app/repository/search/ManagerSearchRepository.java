package com.aujeunecampeur.app.repository.search;

import com.aujeunecampeur.app.domain.Manager;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Manager} entity.
 */
public interface ManagerSearchRepository extends ElasticsearchRepository<Manager, Long> {
}
