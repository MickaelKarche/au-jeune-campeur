package com.aujeunecampeur.app.repository.search;

import com.aujeunecampeur.app.domain.ProductModel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ProductModel} entity.
 */
public interface ProductModelSearchRepository extends ElasticsearchRepository<ProductModel, Long> {
}
