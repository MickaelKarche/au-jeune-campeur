package com.aujeunecampeur.app.repository.search;

import com.aujeunecampeur.app.domain.ShoppingCart;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link ShoppingCart} entity.
 */
public interface ShoppingCartSearchRepository extends ElasticsearchRepository<ShoppingCart, Long> {
}
