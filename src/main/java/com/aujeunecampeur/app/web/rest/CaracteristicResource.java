package com.aujeunecampeur.app.web.rest;

import com.aujeunecampeur.app.domain.Caracteristic;
import com.aujeunecampeur.app.repository.CaracteristicRepository;
import com.aujeunecampeur.app.repository.search.CaracteristicSearchRepository;
import com.aujeunecampeur.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.aujeunecampeur.app.domain.Caracteristic}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CaracteristicResource {

    private final Logger log = LoggerFactory.getLogger(CaracteristicResource.class);

    private static final String ENTITY_NAME = "caracteristic";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaracteristicRepository caracteristicRepository;

    private final CaracteristicSearchRepository caracteristicSearchRepository;

    public CaracteristicResource(CaracteristicRepository caracteristicRepository, CaracteristicSearchRepository caracteristicSearchRepository) {
        this.caracteristicRepository = caracteristicRepository;
        this.caracteristicSearchRepository = caracteristicSearchRepository;
    }

    /**
     * {@code POST  /caracteristics} : Create a new caracteristic.
     *
     * @param caracteristic the caracteristic to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new caracteristic, or with status {@code 400 (Bad Request)} if the caracteristic has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/caracteristics")
    public ResponseEntity<Caracteristic> createCaracteristic(@Valid @RequestBody Caracteristic caracteristic) throws URISyntaxException {
        log.debug("REST request to save Caracteristic : {}", caracteristic);
        if (caracteristic.getId() != null) {
            throw new BadRequestAlertException("A new caracteristic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Caracteristic result = caracteristicRepository.save(caracteristic);
        caracteristicSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/caracteristics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /caracteristics} : Updates an existing caracteristic.
     *
     * @param caracteristic the caracteristic to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caracteristic,
     * or with status {@code 400 (Bad Request)} if the caracteristic is not valid,
     * or with status {@code 500 (Internal Server Error)} if the caracteristic couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/caracteristics")
    public ResponseEntity<Caracteristic> updateCaracteristic(@Valid @RequestBody Caracteristic caracteristic) throws URISyntaxException {
        log.debug("REST request to update Caracteristic : {}", caracteristic);
        if (caracteristic.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Caracteristic result = caracteristicRepository.save(caracteristic);
        caracteristicSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, caracteristic.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /caracteristics} : get all the caracteristics.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of caracteristics in body.
     */
    @GetMapping("/caracteristics")
    public List<Caracteristic> getAllCaracteristics(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Caracteristics");
        return caracteristicRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /caracteristics/:id} : get the "id" caracteristic.
     *
     * @param id the id of the caracteristic to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the caracteristic, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/caracteristics/{id}")
    public ResponseEntity<Caracteristic> getCaracteristic(@PathVariable Long id) {
        log.debug("REST request to get Caracteristic : {}", id);
        Optional<Caracteristic> caracteristic = caracteristicRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(caracteristic);
    }

    /**
     * {@code DELETE  /caracteristics/:id} : delete the "id" caracteristic.
     *
     * @param id the id of the caracteristic to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/caracteristics/{id}")
    public ResponseEntity<Void> deleteCaracteristic(@PathVariable Long id) {
        log.debug("REST request to delete Caracteristic : {}", id);
        caracteristicRepository.deleteById(id);
        caracteristicSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/caracteristics?query=:query} : search for the caracteristic corresponding
     * to the query.
     *
     * @param query the query of the caracteristic search.
     * @return the result of the search.
     */
    @GetMapping("/_search/caracteristics")
    public List<Caracteristic> searchCaracteristics(@RequestParam String query) {
        log.debug("REST request to search Caracteristics for query {}", query);
        return StreamSupport
            .stream(caracteristicSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }

    @GetMapping("/caracteristicsOfProduct/{id}")
    public Optional<Caracteristic[]> getCaracteristicsOfProduct(@PathVariable Long id) {
        log.debug("REST request to get Caracteristic : {}", id);
        return caracteristicRepository.findCaracteristicsOfProduct(id);
    }

}
