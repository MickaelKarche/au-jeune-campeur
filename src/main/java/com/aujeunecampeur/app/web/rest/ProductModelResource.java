package com.aujeunecampeur.app.web.rest;

import com.aujeunecampeur.app.domain.ProductModel;
import com.aujeunecampeur.app.repository.ProductModelRepository;
import com.aujeunecampeur.app.repository.search.ProductModelSearchRepository;
import com.aujeunecampeur.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.aujeunecampeur.app.domain.ProductModel}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProductModelResource {

    private final Logger log = LoggerFactory.getLogger(ProductModelResource.class);

    private static final String ENTITY_NAME = "productModel";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductModelRepository productModelRepository;

    private final ProductModelSearchRepository productModelSearchRepository;

    public ProductModelResource(ProductModelRepository productModelRepository, ProductModelSearchRepository productModelSearchRepository) {
        this.productModelRepository = productModelRepository;
        this.productModelSearchRepository = productModelSearchRepository;
    }

    /**
     * {@code POST  /product-models} : Create a new productModel.
     *
     * @param productModel the productModel to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productModel, or with status {@code 400 (Bad Request)} if the productModel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-models")
    public ResponseEntity<ProductModel> createProductModel(@Valid @RequestBody ProductModel productModel) throws URISyntaxException {
        log.debug("REST request to save ProductModel : {}", productModel);
        if (productModel.getId() != null) {
            throw new BadRequestAlertException("A new productModel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductModel result = productModelRepository.save(productModel);
        productModelSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/product-models/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-models} : Updates an existing productModel.
     *
     * @param productModel the productModel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productModel,
     * or with status {@code 400 (Bad Request)} if the productModel is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productModel couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-models")
    public ResponseEntity<ProductModel> updateProductModel(@Valid @RequestBody ProductModel productModel) throws URISyntaxException {
        log.debug("REST request to update ProductModel : {}", productModel);
        if (productModel.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductModel result = productModelRepository.save(productModel);
        productModelSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, productModel.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-models} : get all the productModels.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productModels in body.
     */
    @GetMapping("/product-models")
    public List<ProductModel> getAllProductModels() {
        log.debug("REST request to get all ProductModels");
        return productModelRepository.findAll();
    }



    /**
     * {@code GET  /product-models/:id} : get the "id" productModel.
     *
     * @param id the id of the productModel to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productModel, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-models/{id}")
    public ResponseEntity<ProductModel> getProductModel(@PathVariable Long id) {
        log.debug("REST request to get ProductModel : {}", id);
        Optional<ProductModel> productModel = productModelRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(productModel);
    }

    /**
     * {@code DELETE  /product-models/:id} : delete the "id" productModel.
     *
     * @param id the id of the productModel to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-models/{id}")
    public ResponseEntity<Void> deleteProductModel(@PathVariable Long id) {
        log.debug("REST request to delete ProductModel : {}", id);
        productModelRepository.deleteById(id);
        productModelSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/product-models?query=:query} : search for the productModel corresponding
     * to the query.
     *
     * @param query the query of the productModel search.
     * @return the result of the search.
     */
    @GetMapping("/_search/product-models")
    public List<ProductModel> searchProductModels(@RequestParam String query) {
        log.debug("REST request to search ProductModels for query {}", query);
        return StreamSupport
            .stream(productModelSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
