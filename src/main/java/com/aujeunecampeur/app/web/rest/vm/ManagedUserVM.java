package com.aujeunecampeur.app.web.rest.vm;

import com.aujeunecampeur.app.service.dto.UserDTO;
import com.aujeunecampeur.app.domain.Customer;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

/**
 * View Model extending the UserDTO, which is meant to be used in the user management UI.
 */
public class ManagedUserVM extends UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 4;

    public static final int PASSWORD_MAX_LENGTH = 100;

    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    private LocalDate birthDate;

    private String phoneNumber;

    public ManagedUserVM() {
        // Empty constructor needed for Jackson.
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
    
    // prettier-ignore
    @Override
    public String toString() {
        return "ManagedUserVM{{" + super.toString() + "}, " + phoneNumber + ", " + birthDate + "} ";
    }
}
