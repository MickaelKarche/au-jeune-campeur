/**
 * View Models used by Spring MVC REST controllers.
 */
package com.aujeunecampeur.app.web.rest.vm;
