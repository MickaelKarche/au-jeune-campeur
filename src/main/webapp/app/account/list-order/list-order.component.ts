import { Component, OnDestroy, OnInit } from '@angular/core';
import { Account } from '../../core/user/account.model';
import { Subscription } from 'rxjs';
import { IOrder, Order } from '../../shared/model/order.model';
import { OrderService } from '../../entities/order/order.service';
import { HttpResponse } from '@angular/common/http';
import { User } from '../../core/user/user.model';
import { UserService } from '../../core/user/user.service';
import { CustomerService } from '../../entities/customer/customer.service';
import { OrderStatus } from '../../shared/model/enumerations/order-status.model';

@Component({
  selector: 'jhi-list-order',
  templateUrl: './list-order.component.html',
  styleUrls: ['./list-order.component.scss'],
})
export class ListOrderComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  currentUser = new User();
  customerId: any;
  listOrder?: IOrder[] | null;
  listCustomerOrder = new Array<Order>();

  constructor(private userService: UserService, private customerService: CustomerService, private orderService: OrderService) {}

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe(value => {
      this.currentUser = value;
      this.customerService.find(this.currentUser.id).subscribe(v => {
        this.customerId = v.body?.id;

        this.orderService.query().subscribe((val: HttpResponse<IOrder[]>) => {
          this.listOrder = val.body;

          if (this.listOrder) {
            this.listOrder.forEach(element => {
              if (element.customer?.id === this.customerId && element.status !== OrderStatus.INPROCESS) {
                this.listCustomerOrder.push(element);
                this.listCustomerOrder.sort((a, b) => b.validationDate!.date() - a.validationDate!.date());
              }
            });
          }
        });
      });
    });
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
