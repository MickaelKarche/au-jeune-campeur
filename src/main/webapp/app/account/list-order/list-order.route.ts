import { Route } from '@angular/router';
import { ListOrderComponent } from './list-order.component';

export const listorderRoute: Route = {
  path: 'order',
  component: ListOrderComponent,
  data: {
    authorities: [],
    pageTitle: 'Listes des Commandes',
  },
};
