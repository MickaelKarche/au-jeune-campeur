import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { UserService } from '../../core/user/user.service';
import { IUser } from '../../core/user/user.model';
import { CustomerService } from '../../entities/customer/customer.service';
import { ICustomer } from '../../shared/model/customer.model';
import { Address, IAddress } from '../../shared/model/address.model';
import { AddressService } from '../../entities/address/address.service';
import { faAddressCard, faTruck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'jhi-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['settings.scss'],
})
export class SettingsComponent implements OnInit {
  account!: Account;
  currentUser!: IUser;
  address!: IAddress;
  currentCustomer!: ICustomer;
  success = false;
  step = 0;
  infoIcon = faAddressCard;
  addressIcon = faTruck;

  settingsForm = this.fb1.group({
    firstName: ['', [Validators.minLength(1), Validators.maxLength(50)]],
    lastName: ['', [Validators.minLength(1), Validators.maxLength(50)]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    phoneNumber: [
      '',
      [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^[0]{1}[1-9]{1}[0-9]{8}$')],
    ],
    birthDate: ['', Validators.required],
  });

  addressForm = this.fb2.group({
    streetNumber: ['', [Validators.minLength(1), Validators.maxLength(4)]],
    street: ['', [Validators.minLength(1), Validators.maxLength(50)]],
    postCode: ['', [Validators.minLength(5), Validators.maxLength(5), Validators.pattern('^[1-9]{1}[0-9]{4}$')]],
    city: ['', [Validators.minLength(1), Validators.maxLength(50)]],
  });

  constructor(
    private accountService: AccountService,
    private fb1: FormBuilder,
    private fb2: FormBuilder,
    private userService: UserService,
    private customerService: CustomerService,
    private addressService: AddressService
  ) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) this.account = account;
    });
    this.userService.getCurrentUser().subscribe(currentUser => {
      if (currentUser) {
        this.settingsForm.patchValue({
          firstName: currentUser.firstName,
          lastName: currentUser.lastName,
          email: currentUser.email,
        });

        this.currentUser = currentUser;
        this.customerService.find(currentUser.id).subscribe(value => {
          this.currentCustomer = value.body as ICustomer;
          if (this.currentCustomer) {
            this.settingsForm.patchValue({
              phoneNumber: this.currentCustomer.phoneNumber,
            });
            this.addressForm.patchValue({
              streetNumber: this.currentCustomer.address?.number,
              street: this.currentCustomer.address?.street,
              postCode: this.currentCustomer.address?.postalCode,
              city: this.currentCustomer.address?.city,
            });
          }
        });
      }
    });
  }

  saveIdentity(): void {
    this.success = false;
    const firstName = this.settingsForm.get('firstName')!.value;
    const lastName = this.settingsForm.get('lastName')!.value;
    const email = this.settingsForm.get('email')!.value;
    const phoneNumber = this.settingsForm.get('phoneNumber')!.value;
    const birthDate = this.settingsForm.get('birthDate')!.value;

    this.account.firstName = firstName;
    this.account.lastName = lastName;
    this.account.email = email;
    this.accountService.save(this.account).subscribe(() => {
      this.success = true;

      this.accountService.authenticate(this.account);
    });
    this.currentCustomer.firstName = firstName;
    this.currentCustomer.lastName = lastName;
    this.currentCustomer.phoneNumber = phoneNumber;
    this.currentCustomer.birthDate = birthDate;
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;
    this.currentUser.email = email;
    this.currentUser.phoneNumber = phoneNumber;
    this.currentUser.birthDate = birthDate;
    this.customerService.update(this.currentCustomer).subscribe();
    this.userService.update(this.currentUser).subscribe();
  }

  private createFromForm(): IAddress {
    return {
      ...new Address(),
      number: this.addressForm.get('streetNumber')!.value,
      city: this.addressForm.get('city')!.value,
      postalCode: this.addressForm.get('postCode')!.value,
      street: this.addressForm.get('street')!.value,
    };
  }

  saveAddress(): void {
    this.address = this.createFromForm();
    this.addressService.create(this.address).subscribe(value => {
      this.address = value.body as IAddress;
      this.currentCustomer.address = this.address;
      this.customerService.update(this.currentCustomer).subscribe();
      this.success = true;
    });
  }

  setStep(index: number): void {
    this.step = index;
  }

  nextStep(): void {
    this.step++;
  }

  prevStep(): void {
    this.step--;
  }
}
