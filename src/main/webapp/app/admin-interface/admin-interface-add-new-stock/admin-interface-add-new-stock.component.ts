import { Component, OnInit } from '@angular/core';
import { IProductModel } from 'app/shared/model/product-model.model';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductService } from 'app/entities/product/product.service';
import { ProductModelService } from 'app/entities/product-model/product-model.service';
import { ActivatedRoute, Params } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { IProduct, Product } from 'app/shared/model/product.model';
import { Observable } from 'rxjs';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';

@Component({
  selector: 'jhi-admin-interface-add-new-stock',
  templateUrl: './admin-interface-add-new-stock.component.html',
  styleUrls: ['./admin-interface-add-new-stock.component.scss'],
})
export class AdminInterfaceAddNewStockComponent implements OnInit {
  productmodel?: IProductModel;
  caracteristics?: ICaracteristic[];
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nbStock: [null, [Validators.required]],
    version: [],
    productModel: [],
    caracteristic: [],
  });

  constructor(
    protected productService: ProductService,
    protected productModelService: ProductModelService,
    protected activatedRoute: ActivatedRoute,
    private caracteristicService: CaracteristicService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.productModelService.find(params['id']).subscribe((value: HttpResponse<IProductModel>) => {
        this.productmodel = value.body as IProductModel;
      });

      this.caracteristicService.query().subscribe((value: HttpResponse<ICaracteristic[]>) => {
        this.caracteristics = value.body as ICaracteristic[];
      });
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const product = this.createFromForm();
    this.subscribeToSaveResponse(this.productService.create(product));
  }

  private createFromForm(): IProduct {
    return {
      ...new Product(),
      id: undefined,
      nbStock: this.editForm.get(['nbStock'])!.value,
      version: 0,
      productModel: this.productmodel,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>): void {
    result.subscribe(
      value => {
        const caracteristic: ICaracteristic = this.editForm.get('caracteristic')!.value;
        caracteristic.products?.push(value.body as IProduct);
        console.log(caracteristic);
        this.caracteristicService.update(caracteristic).subscribe();
        this.onSaveSuccess();
      },
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
