import { NgModule } from '@angular/core';
import { AdminProductViewComponent } from './admin-product-view/admin-product-view.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { adminRoute } from 'app/admin-interface/admin-interface.route';
import { AdminStockViewComponent } from './admin-stock-view/admin-stock-view.component';
import { AdminInterfaceAddNewStockComponent } from './admin-interface-add-new-stock/admin-interface-add-new-stock.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, RouterModule, RouterModule.forChild(adminRoute), FormsModule, ReactiveFormsModule],
  declarations: [AdminProductViewComponent, AdminStockViewComponent, AdminInterfaceAddNewStockComponent],
})
export class AdminInterfaceModule {}
