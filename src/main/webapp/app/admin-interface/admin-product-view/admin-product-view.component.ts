import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IProductModel } from 'app/shared/model/product-model.model';
import { ProductModelService } from 'app/entities/product-model/product-model.service';

@Component({
  selector: 'jhi-admin-product-view',
  templateUrl: './admin-product-view.component.html',
  styleUrls: ['./admin-product-view.component.scss'],
})
export class AdminProductViewComponent implements OnInit {
  productModels?: IProductModel[];
  constructor(private productModelService: ProductModelService) {}

  ngOnInit(): void {
    this.productModelService.query().subscribe((value: HttpResponse<IProductModel[]>) => {
      this.productModels = value.body || [];
      console.log(this.productModels);
      console.log(this.productModels[0].label);
    });
  }
}
