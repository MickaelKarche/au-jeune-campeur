import { Component, OnInit } from '@angular/core';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductModelService } from 'app/entities/product-model/product-model.service';
import { IProductModel } from 'app/shared/model/product-model.model';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';
import { IProductAndCaracteristics, ProductAndCaracteristics } from 'app/shared/model/product-and-caracteristics.model';

@Component({
  selector: 'jhi-admin-stock-view',
  templateUrl: './admin-stock-view.component.html',
  styleUrls: ['./admin-stock-view.component.scss'],
})
export class AdminStockViewComponent implements OnInit {
  products?: IProduct[];
  caracteristics?: Array<ICaracteristic[]>;
  productAndCaracteristics?: IProductAndCaracteristics[];
  productModel?: IProductModel;

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private productModelService: ProductModelService,
    private caracteristicService: CaracteristicService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.getCaracteristicsAndProducts(params['id']);

      this.productModelService.find(params['id']).subscribe((value: HttpResponse<IProductModel>) => {
        this.productModel = value.body as IProductModel;
      });
    });
  }

  getCaracteristicsAndProducts(id: number): void {
    this.productService.getProductsOfProductModel(id).subscribe((value: HttpResponse<IProduct[]>) => {
      this.products = value.body as IProduct[];
      this.caracteristics = [];
      this.productAndCaracteristics = [];
      for (let i = 0; i < this.products.length; i++) {
        this.caracteristicService
          .getCaracteristicsOfProduct(this.products[i].id as number)
          .subscribe((value2: HttpResponse<ICaracteristic[]>) => {
            if (this.productAndCaracteristics && this.products)
              this.productAndCaracteristics.push(new ProductAndCaracteristics(this.products[i], value2.body as ICaracteristic[]));
          });
      }
      console.log(this.productAndCaracteristics);
    });
  }
}
