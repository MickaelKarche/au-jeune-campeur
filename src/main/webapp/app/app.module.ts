import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AuJeuneCampeurSharedModule } from 'app/shared/shared.module';
import { AuJeuneCampeurCoreModule } from 'app/core/core.module';
import { AuJeuneCampeurAppRoutingModule } from './app-routing.module';
import { AuJeuneCampeurHomeModule } from './home/home.module';
import { AuJeuneCampeurEntityModule } from './entities/entity.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import 'hammerjs';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';
import { CustomerInterfaceModule } from 'app/customer-interface/customer-interface.module';

import { NotFoundComponent } from './layouts/not-found/not-found.component';
import { AdminInterfaceModule } from 'app/admin-interface/admin-interface.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    AuJeuneCampeurSharedModule,
    AuJeuneCampeurCoreModule,
    AuJeuneCampeurHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AuJeuneCampeurEntityModule,
    AuJeuneCampeurAppRoutingModule,
    CustomerInterfaceModule,
    AdminInterfaceModule,
  ],

  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent, NotFoundComponent],

  bootstrap: [MainComponent],
})
export class AuJeuneCampeurAppModule {}
