import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import { LoginModalComponent } from 'app/shared/login/login.component';
import { ShoppingCartComponentService } from '../../customer-interface/service/shopping-cart.service';
import { OrderLineComponentService } from '../../customer-interface/service/order-line.service';
import { IOrderLine } from '../../shared/model/order-line.model';
import { NavbarService } from '../../customer-interface/service/navbar.service';
import { AccountService } from 'app/core/auth/account.service';

@Injectable({ providedIn: 'root' })
export class LoginModalService {
  private isOpen = false;
  listOrderLine?: IOrderLine[];

  constructor(
    private modalService: NgbModal,
    private shoppingCartComponentService: ShoppingCartComponentService,
    private navbarComponentService: NavbarService,
    private accountService: AccountService,
    private orderLineComponentService: OrderLineComponentService
  ) {}

  open(): void {
    let nbElement = 0;
    if (this.isOpen) {
      return;
    }
    this.isOpen = true;
    const modalRef: NgbModalRef = this.modalService.open(LoginModalComponent);
    modalRef.result.finally(() => {
      this.isOpen = false;
      if (this.accountService.isAuthenticated()) {
        this.orderLineComponentService.getOrderLine().then(data => {
          this.listOrderLine = data;
          if (this.listOrderLine) {
            this.listOrderLine.forEach(element => {
              nbElement += element.quantity!;
            });
            this.navbarComponentService.nextNbElementShoppingCart(nbElement);
          }
        });
      }
    });
  }
}
