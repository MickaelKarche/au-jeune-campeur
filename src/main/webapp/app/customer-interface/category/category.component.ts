import { Component, OnInit } from '@angular/core';
import { ICategory } from 'app/shared/model/category.model';
import { IProductModel } from 'app/shared/model/product-model.model';
import { CategoryService } from 'app/entities/category/category.service';
import { ActivatedRoute, Params } from '@angular/router';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { NavbarService } from 'app/customer-interface/service/navbar.service';
import { OrderLineComponentService } from 'app/customer-interface/service/order-line.service';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  childrenCategories: ICategory[] | undefined;
  isLeaf: boolean | undefined;
  currentCategory: ICategory | undefined;
  categoryName: String | undefined;
  allCategories?: ICategory[];
  historicsCategories?: ICategory[];
  listProductModel: IProductModel[] | undefined;
  lastCategory?: ICategory;

  constructor(
    private categoryService: CategoryService,
    private navbarComponentService: NavbarService,
    private orderLineComponentService: OrderLineComponentService,
    private accountService: AccountService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.isLeaf = false;

    if (this.accountService.isAuthenticated()) {
      let a = 0;
      this.orderLineComponentService.getOrderLine().then((data: IOrderLine[]) => {
        data.forEach(element => {
          if (element.quantity) {
            a += element.quantity;
            this.navbarComponentService.nextNbElementShoppingCart(a);
          }
        });
      });
    }

    this.activatedRoute.params.subscribe((params: Params) => {
      this.categoryName = params['name'];
      this.categoryService.getCategoryByName(this.categoryName!).subscribe(value => {
        this.currentCategory = value.body as ICategory;

        this.categoryService.getChildrenOfCategory(this.currentCategory.id!).subscribe(value1 => {
          this.childrenCategories = value1.body as ICategory[];
          if (this.childrenCategories.length === 0) {
            this.isLeaf = true;
            this.listProductModel = this.currentCategory?.productModels;
          }

          this.categoryService.query().subscribe(value2 => {
            this.allCategories = value2.body as ICategory[];
            let categoryTmp = this.currentCategory;
            const histCategories = new Array<ICategory>();
            while (categoryTmp?.category) {
              histCategories.push(categoryTmp);
              categoryTmp = categoryTmp.category;
            }
            this.historicsCategories = histCategories.reverse();
            this.lastCategory = this.historicsCategories[this.historicsCategories.length - 1];
            this.historicsCategories.pop();
          });
        });
      });
    });
  }
}
