import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListProductComponent } from './list-product/list-product.component';
import { ProductViewComponent } from 'app/customer-interface/product-view/product-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShoppingCartViewComponent } from './shopping-cart-view/shopping-cart-view.component';
import { OrderPipelineComponent } from './order-pipeline/order-pipeline.component';
import { AuJeuneCampeurSharedModule } from 'app/shared/shared.module';
import { FilterComponent } from './filter/filter.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { RouterModule } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { LastPageComponent } from './last-page/last-page.component';

@NgModule({
  declarations: [
    ListProductComponent,
    ProductViewComponent,
    ShoppingCartViewComponent,
    OrderPipelineComponent,
    FilterComponent,
    SearchBarComponent,
    CategoryComponent,
    LastPageComponent,
  ],
  exports: [ListProductComponent, SearchBarComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, AuJeuneCampeurSharedModule, RouterModule],
})
export class CustomerInterfaceModule {}
