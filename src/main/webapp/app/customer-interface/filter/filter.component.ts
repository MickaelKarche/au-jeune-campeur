import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IProductModel } from 'app/shared/model/product-model.model';
import { ProductService } from 'app/entities/product/product.service';
import { IProduct } from 'app/shared/model/product.model';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  @Input() productModel?: IProductModel | null;
  @Output() caracteristicOutput = new EventEmitter<ICaracteristic>();
  listproduct?: IProduct[] | null;
  caracteristicLabel = ' ';
  listRes = new Array<ICaracteristic>();
  listCaracteristicOfAllProduct?: ICaracteristic[];
  listValue = new Array<String>();
  selectForm = this.formBuilder.group({
    selectCaract: [null, [Validators.required]],
  });

  constructor(
    private productService: ProductService,
    private caracteristicService: CaracteristicService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    const listRightProduct = new Array<IProduct>();
    this.productService.query().subscribe(value => {
      this.listproduct = value.body;
      if (this.listproduct) {
        for (let i = 0; i < this.listproduct.length; i++) {
          if (this.listproduct[i].productModel?.id === this.productModel?.id) {
            listRightProduct?.push(this.listproduct[i]);
          }
        }
      }

      this.caracteristicService.query().subscribe((val: HttpResponse<ICaracteristic[]>) => {
        this.listCaracteristicOfAllProduct = val.body as ICaracteristic[];

        for (let i = 0; i < this.listCaracteristicOfAllProduct.length; i++) {
          // parcours de toutes les caracteristiques
          if (this.listCaracteristicOfAllProduct[i].products) {
            for (let j = 0; j < this.listCaracteristicOfAllProduct[i].products!.length; j++) {
              for (let k = 0; k < listRightProduct.length; k++) {
                if (this.listCaracteristicOfAllProduct[i].products![j].id === listRightProduct[k].id) {
                  this.listRes.push(this.listCaracteristicOfAllProduct[i]);
                }
              }
            }
          }
        }

        this.listRes.sort((a, b) => (b.value as any) - (a.value as any));

        for (let i = 0; i < this.listRes.length; i++) {
          this.listValue.push(this.listRes[i].value!);
        }
        this.caracteristicLabel = this.listRes[0].label!;
      });
    });
  }

  selectCaracteristic(): void {
    this.caracteristicOutput.emit(this.selectForm.get('selectCaract')!.value);
  }
}
