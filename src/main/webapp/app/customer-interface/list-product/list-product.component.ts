import { Component, Input, OnInit } from '@angular/core';
import { ProductModelService } from 'app/entities/product-model/product-model.service';
import { IProductModel } from 'app/shared/model/product-model.model';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { NavbarService } from 'app/customer-interface/service/navbar.service';
import { OrderLineComponentService } from 'app/customer-interface/service/order-line.service';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss'],
})
export class ListProductComponent implements OnInit {
  @Input() listProductModel?: IProductModel[];
  listProductModelSortedByIncreasingPrice = new Array<IProductModel>();
  listProductModelSortedByDecreasingPrice = new Array<IProductModel>();
  whichSort = 'true';

  constructor(
    protected productService: ProductModelService,
    private navbarComponentService: NavbarService,
    private accountService: AccountService,
    private orderLineComponentService: OrderLineComponentService
  ) {}

  ngOnInit(): void {
    if (this.accountService.isAuthenticated()) {
      let c = 0;
      this.orderLineComponentService.getOrderLine().then((data: IOrderLine[]) => {
        data.forEach(element => {
          if (element.quantity) {
            c += element.quantity;
            this.navbarComponentService.nextNbElementShoppingCart(c);
          }
        });
      });
      if (this.listProductModel) this.listProductModel = this.listProductModel.sort((a, b) => a.price! - b.price!);
    }
  }

  sortProductModelByPrice(): void {
    if (this.listProductModel) {
      if (this.whichSort === 'true') {
        this.listProductModel = this.listProductModel.sort((a, b) => a.price! - b.price!);
      } else {
        this.listProductModel = this.listProductModel.sort((a, b) => b.price! - a.price!);
      }
    }
  }
}
