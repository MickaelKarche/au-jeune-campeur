import { Component, OnInit, OnDestroy } from '@angular/core';
import { IOrder, Order } from 'app/shared/model/order.model';
import { OrderService } from 'app/entities/order/order.service';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';
import { CustomerService } from 'app/entities/customer/customer.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Address, IAddress } from 'app/shared/model/address.model';
import { AddressService } from 'app/entities/address/address.service';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { faTruck, faCreditCard } from '@fortawesome/free-solid-svg-icons';

import { Router } from '@angular/router';

import { NavbarService } from '../service/navbar.service';

@Component({
  selector: 'jhi-order-pipeline',
  templateUrl: './order-pipeline.component.html',
  styleUrls: ['./order-pipeline.component.scss'],
})
export class OrderPipelineComponent implements OnInit, OnDestroy {
  listOrder?: IOrder[] | null;
  currentOrder = new Order();
  newAddress = new Address();
  currentUser = new User();
  customerId: any;
  deliveryLastName = '';
  deliveryFirstName = '';
  saveAddress: Boolean | undefined;
  currentCustomer?: ICustomer;
  success: Boolean = false;
  endForm: Boolean = false;
  totalPanier: number | undefined = 0;
  deliveryIcon = faTruck;
  payementIcon = faCreditCard;

  step = 0;

  deliveryAddressForm = this.fb1.group({
    lastName: ['', [Validators.required]],
    firstName: ['', [Validators.required]],
    streetNumber: ['', [Validators.required]],
    street: ['', [Validators.required]],
    postCode: ['', [Validators.required]],
    city: ['', [Validators.required]],
    phoneNumber: [
      '',
      [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('^[0]{1}[1-9]{1}[0-9]{8}$')],
    ],
  });

  paymentForm = this.fb2.group({
    lastNameCard: ['', [Validators.required]],
    firstNameCard: ['', [Validators.required]],
    cardNumber: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
    cvc: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]],
    expirationMonth: [
      '',
      [Validators.required, Validators.minLength(2), Validators.maxLength(2), Validators.pattern('^(0?[1-9]|1[012])$')],
    ],
    expirationYear: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
  });

  constructor(
    private orderService: OrderService,
    private activatedRoute: ActivatedRoute,
    private customerService: CustomerService,
    private addressService: AddressService,
    private userService: UserService,
    private navbarComponentService: NavbarService,
    private fb1: FormBuilder,
    private fb2: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe(value => {
      this.currentUser = value;
      this.customerService.find(this.currentUser.id).subscribe(v => {
        this.customerId = v.body?.id;

        this.customerService.find(this.customerId).subscribe(value1 => {
          this.currentCustomer = value1.body as ICustomer;

          this.deliveryAddressForm.patchValue({
            lastName: this.currentUser.lastName,
            firstName: this.currentUser.firstName,
            streetNumber: this.currentCustomer.address?.number,
            street: this.currentCustomer.address?.street,
            postCode: this.currentCustomer.address?.postalCode,
            city: this.currentCustomer.address?.city,
            phoneNumber: this.currentCustomer.phoneNumber,
          });

          this.orderService.query().subscribe((val: HttpResponse<IOrder[]>) => {
            this.listOrder = val.body;

            if (this.listOrder) {
              for (let i = 0; i < this.listOrder?.length; i++) {
                if (this.listOrder[i].customer?.id === this.customerId && this.listOrder[i].status === OrderStatus.INPROCESS) {
                  this.currentOrder = this.listOrder[i];
                  this.totalPanier = this.currentOrder.shoppingCart?.totalPrice;
                  break;
                }
              }
            }
          });
        });
      });
    });
  }

  submit(): void {
    this.newAddress = this.createFromForm();
    this.addressService.create(this.newAddress).subscribe(value => {
      this.newAddress = value.body as IAddress;
      this.currentOrder.deliveryAddress = this.newAddress;
      if (!this.checkDeliveryUser()) {
        this.currentOrder.deliveryLastName = this.deliveryAddressForm.get('lastName')!.value;
        this.currentOrder.deliveryFirstName = this.deliveryAddressForm.get('firstName')!.value;
      }
      this.orderService.update(this.currentOrder).subscribe();
      if (this.saveAddress) {
        if (this.currentCustomer) {
          this.currentCustomer.address = this.newAddress;
          this.customerService.update(this.currentCustomer).subscribe();
        }
      }
    });
    this.success = true;
    setTimeout(() => {
      this.success = false;
    }, 4000);
  }

  // crée une nouvelle adresse en fonction du formulaire
  private createFromForm(): IAddress {
    return {
      ...new Address(),
      number: this.deliveryAddressForm.get('streetNumber')!.value,
      city: this.deliveryAddressForm.get('city')!.value,
      postalCode: this.deliveryAddressForm.get('postCode')!.value,
      street: this.deliveryAddressForm.get('street')!.value,
    };
  }

  // renvoie true si le nom et prénom du formulaire sont les meme que ceux du client connecté

  private checkDeliveryUser(): Boolean {
    if (
      this.currentOrder.deliveryLastName === this.deliveryAddressForm.get('lastName')!.value &&
      this.currentOrder.deliveryFirstName === this.deliveryAddressForm.get('firstName')!.value
    ) {
      return true;
    }
    return false;
  }

  submitPayment(): void {
    this.currentOrder.status = OrderStatus.PAID;
    this.orderService.update(this.currentOrder).subscribe();
    this.endForm = true;
    this.router.navigate(['/fin']);
  }

  setStep(index: number): void {
    this.step = index;
  }

  nextStep(): void {
    this.step++;
    if (this.step === 1) {
      this.success = true;
      setTimeout(() => {
        this.success = false;
      }, 4000);
    } else {
      this.endForm = true;
    }
  }

  prevStep(): void {
    this.step--;
  }

  ngOnDestroy(): void {
    this.navbarComponentService.nextIsCheckingOut(false);
  }
}
