import { Component, Input, OnInit } from '@angular/core';
import { IProductModel } from 'app/shared/model/product-model.model';
import { HttpResponse } from '@angular/common/http';
import { ProductModelService } from 'app/entities/product-model/product-model.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CustomerComponentService } from 'app/customer-interface/service/customer.service';
import { ShoppingCartComponentService } from 'app/customer-interface/service/shopping-cart.service';
import { OrderComponentService } from 'app/customer-interface/service/order.service';
import { OrderLineComponentService } from 'app/customer-interface/service/order-line.service';
import { ProductComponentService } from 'app/customer-interface/service/product.service';
import { IOrder } from 'app/shared/model/order.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { IProduct } from 'app/shared/model/product.model';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { NavbarService } from '../service/navbar.service';
import { IOrderLine } from '../../shared/model/order-line.model';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
})
export class ProductViewComponent implements OnInit {
  @Input() productModel?: IProductModel | null;
  caracteristic?: ICaracteristic;
  quantity = 1;
  id = 0;
  addedToShopCart = false;
  notEnoughStock = false;

  constructor(
    protected productModelService: ProductModelService,
    private activatedRoute: ActivatedRoute,
    private customerComponentService: CustomerComponentService,
    private shoppingCartComponentService: ShoppingCartComponentService,
    private orderComponentService: OrderComponentService,
    private orderLineComponentService: OrderLineComponentService,
    private productComponentService: ProductComponentService,
    private caracteristicService: CaracteristicService,
    private navbarComponentService: NavbarService,
    private accountService: AccountService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
    });
    this.productModelService.find(this.id).subscribe((value: HttpResponse<IProductModel>) => {
      this.productModel = value.body;
      if (!value.body) {
        this.router.navigate(['404']);
      }
      if (this.accountService.isAuthenticated()) {
        let a = 0;
        this.orderLineComponentService.getOrderLine().then((data: IOrderLine[]) => {
          data.forEach(element => {
            if (element.quantity) {
              a += element.quantity;
              this.navbarComponentService.nextNbElementShoppingCart(a);
            }
          });
        });
      }

      this.caracteristicService.query().subscribe((value2: HttpResponse<ICaracteristic[]>) => {
        const listcaract = value2.body as ICaracteristic[];
        for (let i = 0; i < listcaract.length; i++) {
          if ((listcaract[i].products as IProduct)[0].productModel?.id === this.productModel?.id) {
            this.caracteristic = listcaract[i];
          }
        }
      });
    });
  }

  addProduct(): void {
    if (this.caracteristic)
      this.caracteristicService.find(this.caracteristic.id as number).subscribe((value: HttpResponse<ICaracteristic>) => {
        if (this.productModel) {
          const caracteristic = value.body as ICaracteristic;
          const product = this.productComponentService.getCurrentProduct(caracteristic, this.productModel);
          console.log(this.quantity, product.nbStock);
          if (product.nbStock && this.quantity <= product.nbStock) {
            this.productComponentService.tryToReserveStock(product, this.quantity).then(answer => {
              this.addedToShopCart = answer;
              if (answer) {
                this.orderComponentService.getAllOrder().then((listOrder: IOrder[]) => {
                  this.customerComponentService.getCustomer().then((customer: ICustomer) => {
                    if (this.productModel)
                      if (!this.shoppingCartComponentService.hasAShoppingCart(listOrder, customer)) {
                        this.orderLineComponentService.createAnOrder(product, this.quantity);
                      } else {
                        const shopCart = this.shoppingCartComponentService.getActualShoppingCart(listOrder, customer);
                        this.orderLineComponentService.createOrUpdate(product, shopCart, this.quantity);
                      }
                  });
                });
              }
            });
          } else {
            this.notEnoughStock = true;
          }
        }
      });
  }

  getCaracteristic(caracteristic: ICaracteristic): void {
    this.caracteristic = caracteristic;
    console.log(this.caracteristic);
  }

  previousState(): void {
    window.history.back();
  }
}
