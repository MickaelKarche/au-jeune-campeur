import { Component, OnInit } from '@angular/core';
import { Search } from 'app/shared/util/request-util';
import { ProductModelService } from 'app/entities/product-model/product-model.service';
import { IProductModel } from 'app/shared/model/product-model.model';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  searchWord = '';
  productsFound!: IProductModel[];
  selectedProduct: IProductModel | undefined;

  constructor(private productModelService: ProductModelService, private router: Router) {}

  ngOnInit(): void {
    this.productsFound = [];
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => (term === '' ? [] : this.productsFound?.slice(0, 10)))
    );

  formatter = (x: { label: string }) => x.label;

  onKey(event: KeyboardEvent): void {
    this.searchWord = (event.target as HTMLInputElement).value;
    if (this.searchWord.length > 1) {
      this.searchWithElastic();
    }
  }

  searchWithElastic(): void {
    const req: Search = { query: '*' + this.searchWord + '*' };
    this.productModelService.search(req).subscribe(value => {
      this.productsFound = value.body as IProductModel[];
    });
  }

  goToDetails(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = function (): boolean {
      return false;
    };
    this.router.onSameUrlNavigation = 'reload';
    if (this.selectedProduct?.id) {
      this.router.navigate(['/p', this.selectedProduct?.id]);
    }
  }
}
