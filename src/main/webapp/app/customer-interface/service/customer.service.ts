import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer/customer.service';
import { UserService } from 'app/core/user/user.service';

@Injectable({
  providedIn: 'root',
})
export class CustomerComponentService {
  constructor(private userService: UserService, private customerService: CustomerService) {}

  getCustomer(): Promise<any> {
    return new Promise(resolve => {
      this.userService.getCurrentUser().subscribe(user => {
        this.customerService.query().subscribe((value: HttpResponse<ICustomer[]>) => {
          for (let i = 0; i < (value.body as ICustomer[]).length; i++) {
            if ((value.body as ICustomer[])[i].id === user.id) {
              resolve((value.body as ICustomer[])[i]);
              break;
            }
          }
        });
      });
    });
  }
}
