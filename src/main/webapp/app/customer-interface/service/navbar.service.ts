import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NavbarService {
  private nbElementShoppingCart = new BehaviorSubject<number>(0);
  sharedNumber = this.nbElementShoppingCart.asObservable();
  private isCheckingOut = new BehaviorSubject<Boolean>(false);
  sharedBoolean = this.isCheckingOut.asObservable();

  constructor() {}

  nextNbElementShoppingCart(number: number): void {
    this.nbElementShoppingCart.next(number);
  }

  getValueNbElementShoppingCart(): number {
    return this.nbElementShoppingCart.getValue();
  }

  nextIsCheckingOut(bool: Boolean): void {
    this.isCheckingOut.next(bool);
  }
}
