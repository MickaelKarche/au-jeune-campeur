import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IOrderLine, OrderLine } from 'app/shared/model/order-line.model';
import { IProduct } from 'app/shared/model/product.model';
import { OrderLineService } from 'app/entities/order-line/order-line.service';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ProductService } from 'app/entities/product/product.service';
import { CustomerComponentService } from 'app/customer-interface/service/customer.service';
import { OrderComponentService } from 'app/customer-interface/service/order.service';
import { ProductComponentService } from 'app/customer-interface/service/product.service';
import { IOrder } from 'app/shared/model/order.model';
import { OrderService } from 'app/entities/order/order.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { ShoppingCartComponentService } from 'app/customer-interface/service/shopping-cart.service';

@Injectable({
  providedIn: 'root',
})
export class OrderLineComponentService {
  constructor(
    private orderLineService: OrderLineService,
    private shoppingCartComponentService: ShoppingCartComponentService,
    private productService: ProductService,
    private customerComponentService: CustomerComponentService,
    private orderComponentService: OrderComponentService,
    private productComponentService: ProductComponentService,
    private orderService: OrderService
  ) {}

  hasProductInShopCart(shopCart: IShoppingCart, currentProduct: IProduct): Promise<any> {
    return new Promise(resolve => {
      let hasProductInCart: Boolean = false;
      this.orderLineService.query().subscribe((value: HttpResponse<IOrderLine[]>) => {
        const tmp = value.body as IOrderLine[];
        for (let i = 0; i < tmp.length; i++) {
          if (tmp[i].shoppingCart?.id === shopCart.id && tmp[i].product?.id === currentProduct.id) {
            hasProductInCart = true;
            break;
          }
        }
        resolve(hasProductInCart);
      });
    });
  }

  updateExistingOrderLine(currentProduct: IProduct, shopCart: IShoppingCart, quantity: number): void {
    this.orderLineService.query().subscribe((value: HttpResponse<IOrderLine[]>) => {
      const tmp = value.body as IOrderLine[];
      for (let i = 0; i < tmp.length; i++) {
        if (tmp[i].shoppingCart?.id === shopCart?.id && tmp[i].product?.id === currentProduct?.id) {
          const orderLine: IOrderLine = tmp[i];
          if (orderLine.quantity) orderLine.quantity = orderLine.quantity + quantity;
          if ((orderLine.priceLine || orderLine.priceLine === 0) && currentProduct.productModel?.price)
            orderLine.priceLine = orderLine.priceLine + currentProduct.productModel.price * quantity;
          this.orderLineService.update(orderLine).subscribe(() => {
            this.shoppingCartComponentService.updateTotalPrice(shopCart);
          });
          break;
        }
      }
    });
  }

  createNewOrderLine(currentProduct: IProduct, shopCart: IShoppingCart, quantity: number): void {
    console.log(currentProduct);
    if (currentProduct.productModel?.price) {
      if (shopCart) {
        const orderLine: IOrderLine = new OrderLine();
        orderLine.product = currentProduct;
        orderLine.quantity = quantity;
        orderLine.priceLine = currentProduct.productModel.price * quantity;
        orderLine.shoppingCart = shopCart;
        this.orderLineService.create(orderLine).subscribe(() => {
          this.shoppingCartComponentService.updateTotalPrice(shopCart);
        });
      }
    }
  }

  createOrUpdate(product: IProduct, shopCart: IShoppingCart, quantity: number): void {
    this.hasProductInShopCart(shopCart, product).then((value: Boolean) => {
      console.log(value);
      if (value) {
        this.updateExistingOrderLine(product, shopCart, quantity);
      } else {
        this.createNewOrderLine(product, shopCart, quantity);
      }
    });
  }

  getOrderLine(): Promise<any> {
    return new Promise(resolve => {
      this.orderComponentService.getCurrentShoppingCart().then((order: IOrder) => {
        const listOrderLineToReturn: IOrderLine[] = [];
        this.orderLineService.query().subscribe((value: HttpResponse<IOrderLine[]>) => {
          const listOrderLine = value.body as IOrderLine[];
          for (let i = 0; i < listOrderLine.length; i++) {
            if (listOrderLine[i].shoppingCart?.id === order?.shoppingCart?.id) {
              listOrderLineToReturn.push(listOrderLine[i]);
            }
          }
          resolve(listOrderLineToReturn);
        });
      });
    });
  }

  deleteOrderLine(orderLine: IOrderLine): Promise<boolean> {
    return new Promise(resolve => {
      this.orderLineService.find(orderLine.id as number).subscribe((orderLineHttpResponse: HttpResponse<IOrderLine>) => {
        const currentProduct = (orderLineHttpResponse.body as IOrderLine).product;
        if (orderLine.id && currentProduct) {
          this.shoppingCartComponentService.deleteOrderLineToShoppingCart(orderLine);
          this.productComponentService.changeStockWhenOrderLineDeleted(orderLine, currentProduct).then(isDeleted => {
            this.orderLineService.delete(orderLine.id as number).subscribe(() => {
              resolve(isDeleted);
            });
          });
        }
      });
    });
  }

  changeQuantity(orderLine: IOrderLine): Promise<any> {
    return new Promise<any>(resolve => {
      this.orderLineService.find(orderLine.id as number).subscribe((orderLineHttpResponse: HttpResponse<IOrderLine>) => {
        const currentProduct = (orderLineHttpResponse.body as IOrderLine).product;

        if (orderLine.id)
          this.orderLineService.find(orderLine.id).subscribe((value: HttpResponse<IOrderLine>) => {
            const tmpOldQuantity = (value.body as IOrderLine).quantity;
            if (orderLine.id && (tmpOldQuantity || tmpOldQuantity === 0) && currentProduct) {
              this.productComponentService.changeStockWhenOrderLineChanged(orderLine, tmpOldQuantity, currentProduct).then(stockChanged => {
                if (stockChanged) {
                  if ((orderLine.quantity || orderLine.quantity === 0) && orderLine.product?.productModel?.price) {
                    orderLine.priceLine = orderLine.product?.productModel?.price * orderLine.quantity;
                  }
                  this.orderLineService.update(orderLine).subscribe(() => {
                    this.shoppingCartComponentService
                      .updateTotalPrice(orderLine.shoppingCart as IShoppingCart)
                      .then((shopCart: IShoppingCart) => {
                        resolve(shopCart);
                      });
                  });
                } else {
                  this.shoppingCartComponentService
                    .updateTotalPrice(orderLine.shoppingCart as IShoppingCart)
                    .then((shopCart: IShoppingCart) => {
                      resolve(shopCart);
                    });
                }
              });
            }
          });
      });
    });
  }

  createAnOrder(product: IProduct, quantity: number): void {
    this.customerComponentService.getCustomer().then((customer: ICustomer) => {
      const order = this.orderComponentService.initInProcessOrder(customer);
      let shopCart: IShoppingCart;
      this.orderService.create(order).subscribe((value: HttpResponse<IOrder>) => {
        const newOrder = value.body as IOrder;
        shopCart = newOrder.shoppingCart as IShoppingCart;
        this.createOrUpdate(product, shopCart, quantity);
      });
    });
  }
}
