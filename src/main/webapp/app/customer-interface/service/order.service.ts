import { Injectable } from '@angular/core';
import { IOrder, Order } from 'app/shared/model/order.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';
import { ICustomer } from 'app/shared/model/customer.model';
import * as moment from 'moment';
import { ShoppingCart } from 'app/shared/model/shopping-cart.model';
import { OrderService } from 'app/entities/order/order.service';
import { HttpResponse } from '@angular/common/http';
import { CustomerComponentService } from 'app/customer-interface/service/customer.service';

@Injectable({
  providedIn: 'root',
})
export class OrderComponentService {
  constructor(private orderService: OrderService, private customerComponentService: CustomerComponentService) {}

  initInProcessOrder(customer: ICustomer): IOrder {
    const tmp: IOrder = new Order();
    tmp.status = OrderStatus.INPROCESS;
    tmp.validationDate = moment().local();
    tmp.deliveryFirstName = customer.firstName;
    tmp.deliveryLastName = customer.lastName;
    tmp.customer = customer;
    tmp.shoppingCart = new ShoppingCart();
    tmp.shoppingCart.totalPrice = 0;
    return tmp;
  }

  getAllOrder(): Promise<any> {
    return new Promise(resolve => {
      this.orderService.query().subscribe((value: HttpResponse<IOrder[]>) => {
        resolve(value.body as IOrder[]);
      });
    });
  }

  getCurrentShoppingCart(): Promise<any> {
    return new Promise(resolve => {
      this.customerComponentService.getCustomer().then((customer: ICustomer) => {
        this.orderService.query().subscribe((value: HttpResponse<IOrder[]>) => {
          const listOrder = value.body as IOrder[];
          for (let i = 0; i < listOrder.length; i++) {
            if (listOrder[i].customer?.id === customer.id && listOrder[i].status === OrderStatus.INPROCESS) {
              resolve(listOrder[i]);
              break;
            }
          }
        });
      });
    });
  }
}
