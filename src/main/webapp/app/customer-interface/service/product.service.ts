import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { IProduct, Product } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';
import { IProductModel, ProductModel } from 'app/shared/model/product-model.model';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';

@Injectable({
  providedIn: 'root',
})
export class ProductComponentService {
  constructor(private productService: ProductService) {}

  minusStockOfProduct(productModel: ProductModel, quantity: number): void {
    this.productService.query().subscribe((value: HttpResponse<IProduct[]>) => {
      const listProducts = value.body as IProduct[];
      let actualProduct: IProduct = new Product();
      for (let i = 0; i < listProducts.length; i++) {
        if (listProducts[i].productModel?.id === productModel?.id) {
          actualProduct = listProducts[i];
          break;
        }
      }
      console.log(actualProduct);
      if (actualProduct.nbStock && actualProduct.nbStock > quantity) {
        actualProduct.nbStock = actualProduct.nbStock - quantity;
        this.productService.update(actualProduct).subscribe();
      }
    });
  }

  changeStockWhenOrderLineDeleted(orderLine: IOrderLine, currentProduct: IProduct): Promise<any> {
    return new Promise<boolean>(resolve => {
      if (currentProduct.id)
        this.productService.find(currentProduct.id).subscribe((value: HttpResponse<IProduct>) => {
          const product = value.body as IProduct;
          if (
            (product.nbStock || product.nbStock === 0) &&
            (product.version || product.version === 0) &&
            currentProduct.version === product.version
          ) {
            const tmpProduct = orderLine.product;
            if ((tmpProduct?.nbStock || tmpProduct?.nbStock === 0) && orderLine.quantity && tmpProduct.version) {
              tmpProduct.nbStock = tmpProduct.nbStock + orderLine.quantity;
              tmpProduct.version++;
            }
            this.productService.update(tmpProduct as IProduct).subscribe(() => resolve(true));
          }
          resolve(false);
        });
    });
  }

  changeStockWhenOrderLineChanged(orderLine: IOrderLine, tmpOldQuantity: number, currentProduct: IProduct): Promise<Boolean> {
    return new Promise<boolean>(resolve => {
      if (orderLine.product?.id)
        this.productService.find(orderLine.product?.id).subscribe((value: HttpResponse<IProduct>) => {
          const product = value.body as IProduct;
          if (
            (product.nbStock || product.nbStock === 0) &&
            (orderLine.quantity || orderLine.quantity === 0) &&
            (tmpOldQuantity || tmpOldQuantity === 0) &&
            product.nbStock - (orderLine.quantity - tmpOldQuantity) >= 0 &&
            product.version === currentProduct.version &&
            product.version
          ) {
            product.nbStock = product.nbStock - (orderLine.quantity - tmpOldQuantity);
            product.version++;
            this.productService.update(product).subscribe();
            resolve(true);
          }
          resolve(false);
        });
    });
  }

  getCurrentProduct(caracteristic: ICaracteristic, productModel: IProductModel): IProduct {
    for (let i = 0; caracteristic.products && i < caracteristic.products.length; i++) {
      if (caracteristic.products[i].productModel?.id === productModel.id) {
        console.log(caracteristic.products[i]);
        return caracteristic.products[i];
      }
    }
    return new Product();
  }

  tryToReserveStock(currentProduct: IProduct, quantity: number): Promise<boolean> {
    return new Promise<boolean>(resolve => {
      if (currentProduct.id)
        this.productService.find(currentProduct.id).subscribe((value: HttpResponse<IProduct>) => {
          const product = value.body as IProduct;
          if (product.nbStock && (product.version || product.version === 0) && currentProduct.version === product.version) {
            product.nbStock = product.nbStock - quantity;
            product.version++;
            this.productService.update(product).subscribe(() => resolve(true));
          } else {
            resolve(false);
          }
        });
    });
  }

  // startTransaction(): number {}
}
