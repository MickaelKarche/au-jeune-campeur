import { Injectable } from '@angular/core';
import { IOrder } from 'app/shared/model/order.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { IShoppingCart, ShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ShoppingCartService } from 'app/entities/shopping-cart/shopping-cart.service';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { OrderLineService } from 'app/entities/order-line/order-line.service';
import { HttpResponse } from '@angular/common/http';
import { NavbarService } from './navbar.service';

@Injectable({
  providedIn: 'root',
})
export class ShoppingCartComponentService {
  constructor(
    private shoppingCartService: ShoppingCartService,
    private navbarService: NavbarService,
    private orderLineService: OrderLineService
  ) {}

  hasAShoppingCart(listOrder: IOrder[], customer: ICustomer): Boolean {
    let a = 0;
    for (let i = 0; i < listOrder.length; i++) {
      if (listOrder[i].customer?.id === customer.id && listOrder[i].status === OrderStatus.INPROCESS) {
        if (listOrder[i].shoppingCart && listOrder[i].shoppingCart!.orderLines) {
          listOrder[i].shoppingCart!.orderLines!.forEach(element => {
            a += element.quantity!;
          });
          this.navbarService.nextNbElementShoppingCart(a);
        }
        return true;
      }
    }
    return false;
  }

  getActualShoppingCart(listOrder: IOrder[], customer: ICustomer): IShoppingCart {
    for (let i = 0; i < listOrder.length; i++) {
      if (listOrder[i].customer?.id === customer.id && listOrder[i].status === OrderStatus.INPROCESS) {
        return listOrder[i].shoppingCart as IShoppingCart;
      }
    }
    return new ShoppingCart();
  }

  deleteOrderLineToShoppingCart(orderLine: IOrderLine): void {
    const tmpShopCart = orderLine.shoppingCart;
    if (tmpShopCart?.totalPrice && orderLine.quantity && orderLine.product?.productModel?.price) {
      tmpShopCart.totalPrice = tmpShopCart?.totalPrice - orderLine.product?.productModel?.price * orderLine.quantity;
      this.navbarService.nextNbElementShoppingCart(this.navbarService.getValueNbElementShoppingCart() - orderLine.quantity);
    }
    this.shoppingCartService.update(tmpShopCart as IShoppingCart).subscribe();
  }

  updateTotalPrice(shopCart: IShoppingCart): Promise<any> {
    return new Promise(resolve => {
      this.orderLineService.query().subscribe((value: HttpResponse<IOrderLine[]>) => {
        const listOrderLine = value.body as IOrderLine[];
        let total = 0,
          nbElement = 0;
        console.log(shopCart);
        console.log(listOrderLine);
        for (let i = 0; i < listOrderLine.length; i++) {
          if (listOrderLine[i].shoppingCart?.id === shopCart?.id) {
            console.log('fonctionne');
            total = total + (listOrderLine[i].priceLine as number);
            nbElement += listOrderLine[i].quantity as number;
            console.log(listOrderLine[i].priceLine);
          }
        }
        shopCart.totalPrice = total;
        this.shoppingCartService.update(shopCart).subscribe();
        this.navbarService.nextNbElementShoppingCart(nbElement);
        console.log('flop');
        console.log(shopCart);
        resolve(shopCart);
      });
    });
  }
}
