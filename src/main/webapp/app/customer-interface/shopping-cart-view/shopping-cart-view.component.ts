import { Component, OnDestroy, OnInit } from '@angular/core';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { OrderLineComponentService } from 'app/customer-interface/service/order-line.service';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ShoppingCartComponentService } from 'app/customer-interface/service/shopping-cart.service';
import { OrderComponentService } from 'app/customer-interface/service/order.service';
import { IOrder } from 'app/shared/model/order.model';
import { faTrashAlt, faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { HttpResponse } from '@angular/common/http';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';
import { IProduct } from 'app/shared/model/product.model';
import { IOrderLineAndCaracteristic, OrderLineAndCaracteristic } from 'app/shared/model/order-line-and-caracteristic.model';
import { NavbarService } from '../service/navbar.service';

@Component({
  selector: 'jhi-shopping-cart-view',
  templateUrl: './shopping-cart-view.component.html',
  styleUrls: ['./shopping-cart-view.component.scss'],
})
export class ShoppingCartViewComponent implements OnInit, OnDestroy {
  listOrderLine?: IOrderLine[];
  shoppingCart?: IShoppingCart;
  isDeleted = false;
  thrashIcon = faTrashAlt;
  arrowLeftIcon = faArrowLeft;
  arrowRightIcon = faArrowRight;
  displayedColumns: string[] = ['product?', 'caracteristic', 'price', 'quantity', 'subtotal', 'deleteButton'];
  orderLineAndCaracteristics?: IOrderLineAndCaracteristic[];
  boolIsCheckingOut: Boolean | undefined;

  constructor(
    private orderLineComponentService: OrderLineComponentService,
    private shoppingCartComponentService: ShoppingCartComponentService,
    private navbarComponentService: NavbarService,
    private orderComponentService: OrderComponentService,
    private caracteristicService: CaracteristicService
  ) {}

  ngOnInit(): void {
    this.navbarComponentService.sharedBoolean.subscribe(boolElement => (this.boolIsCheckingOut = boolElement));
    this.navbarComponentService.nextIsCheckingOut(true);
    this.orderLineComponentService.getOrderLine().then((data: IOrderLine[]) => {
      this.listOrderLine = data;

      this.initOrderlineAndCaract();
      this.orderComponentService.getCurrentShoppingCart().then((order: IOrder) => {
        this.shoppingCartComponentService.updateTotalPrice(order.shoppingCart as IShoppingCart).then(value => (this.shoppingCart = value));
      });
    });
  }

  public initOrderlineAndCaract(): void {
    this.caracteristicService.query().subscribe((value: HttpResponse<ICaracteristic[]>) => {
      this.orderLineAndCaracteristics = [];
      const allCaract = value.body as ICaracteristic[];
      for (let i = 0; i < allCaract.length; i++) {
        for (let k = 0; k < (allCaract[i].products as IProduct[]).length; k++) {
          for (let j = 0; this.listOrderLine && j < this.listOrderLine?.length; j++) {
            if ((this.listOrderLine[j].product as IProduct).id === (allCaract[i].products as IProduct[])[k].id) {
              this.orderLineAndCaracteristics.push(new OrderLineAndCaracteristic(allCaract[i], this.listOrderLine[j]));
            }
          }
        }
      }
      console.log(this.orderLineAndCaracteristics);
    });
  }

  public deleteOrderline(orderLine: IOrderLine): void {
    this.orderLineComponentService.deleteOrderLine(orderLine).then((isDeleted: boolean) => {
      this.orderLineComponentService.getOrderLine().then(data => {
        this.listOrderLine = data;

        this.initOrderlineAndCaract();
        this.isDeleted = isDeleted;
        this.orderComponentService.getCurrentShoppingCart().then((order: IOrder) => {
          this.shoppingCartComponentService
            .updateTotalPrice(order.shoppingCart as IShoppingCart)
            .then(value => (this.shoppingCart = value));
        });
      });
    });
  }

  public changeQuantity(orderLine: IOrderLine): void {
    console.log(orderLine.quantity);
    this.orderLineComponentService.changeQuantity(orderLine).then(shopCart => {
      this.shoppingCart = shopCart;
      this.orderLineComponentService.getOrderLine().then(data => {
        this.listOrderLine = data;
        this.initOrderlineAndCaract();
      });
    });
  }

  previousState(): void {
    window.history.back();
  }

  ngOnDestroy(): void {
    this.navbarComponentService.nextIsCheckingOut(false);
  }
}
