import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICaracteristic } from 'app/shared/model/caracteristic.model';
import { CaracteristicService } from './caracteristic.service';

@Component({
  templateUrl: './caracteristic-delete-dialog.component.html',
})
export class CaracteristicDeleteDialogComponent {
  caracteristic?: ICaracteristic;

  constructor(
    protected caracteristicService: CaracteristicService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.caracteristicService.delete(id).subscribe(() => {
      this.eventManager.broadcast('caracteristicListModification');
      this.activeModal.close();
    });
  }
}
