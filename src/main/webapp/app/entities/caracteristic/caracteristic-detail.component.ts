import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaracteristic } from 'app/shared/model/caracteristic.model';

@Component({
  selector: 'jhi-caracteristic-detail',
  templateUrl: './caracteristic-detail.component.html',
})
export class CaracteristicDetailComponent implements OnInit {
  caracteristic: ICaracteristic | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caracteristic }) => (this.caracteristic = caracteristic));
  }

  previousState(): void {
    window.history.back();
  }
}
