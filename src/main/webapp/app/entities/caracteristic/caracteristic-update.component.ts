import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICaracteristic, Caracteristic } from 'app/shared/model/caracteristic.model';
import { CaracteristicService } from './caracteristic.service';
import { IProduct } from 'app/shared/model/product.model';
import { ProductService } from 'app/entities/product/product.service';

@Component({
  selector: 'jhi-caracteristic-update',
  templateUrl: './caracteristic-update.component.html',
})
export class CaracteristicUpdateComponent implements OnInit {
  isSaving = false;
  products: IProduct[] = [];

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required]],
    value: [null, [Validators.required]],
    products: [],
  });

  constructor(
    protected caracteristicService: CaracteristicService,
    protected productService: ProductService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caracteristic }) => {
      this.updateForm(caracteristic);

      this.productService.query().subscribe((res: HttpResponse<IProduct[]>) => (this.products = res.body || []));
    });
  }

  updateForm(caracteristic: ICaracteristic): void {
    this.editForm.patchValue({
      id: caracteristic.id,
      label: caracteristic.label,
      value: caracteristic.value,
      products: caracteristic.products,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caracteristic = this.createFromForm();
    console.log(caracteristic);
    if (caracteristic.id !== undefined) {
      this.subscribeToSaveResponse(this.caracteristicService.update(caracteristic));
    } else {
      this.subscribeToSaveResponse(this.caracteristicService.create(caracteristic));
    }
  }

  private createFromForm(): ICaracteristic {
    return {
      ...new Caracteristic(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      value: this.editForm.get(['value'])!.value,
      products: this.editForm.get(['products'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaracteristic>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IProduct): any {
    return item.id;
  }

  getSelected(selectedVals: IProduct[], option: IProduct): IProduct {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
