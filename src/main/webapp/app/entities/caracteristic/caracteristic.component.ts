import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaracteristic } from 'app/shared/model/caracteristic.model';
import { CaracteristicService } from './caracteristic.service';
import { CaracteristicDeleteDialogComponent } from './caracteristic-delete-dialog.component';

@Component({
  selector: 'jhi-caracteristic',
  templateUrl: './caracteristic.component.html',
})
export class CaracteristicComponent implements OnInit, OnDestroy {
  caracteristics?: ICaracteristic[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected caracteristicService: CaracteristicService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.caracteristicService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<ICaracteristic[]>) => (this.caracteristics = res.body || []));
      return;
    }

    this.caracteristicService.query().subscribe((res: HttpResponse<ICaracteristic[]>) => (this.caracteristics = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCaracteristics();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICaracteristic): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCaracteristics(): void {
    this.eventSubscriber = this.eventManager.subscribe('caracteristicListModification', () => this.loadAll());
  }

  delete(caracteristic: ICaracteristic): void {
    const modalRef = this.modalService.open(CaracteristicDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.caracteristic = caracteristic;
  }
}
