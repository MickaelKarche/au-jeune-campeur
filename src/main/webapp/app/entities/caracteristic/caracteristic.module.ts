import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuJeuneCampeurSharedModule } from 'app/shared/shared.module';
import { CaracteristicComponent } from './caracteristic.component';
import { CaracteristicDetailComponent } from './caracteristic-detail.component';
import { CaracteristicUpdateComponent } from './caracteristic-update.component';
import { CaracteristicDeleteDialogComponent } from './caracteristic-delete-dialog.component';
import { caracteristicRoute } from './caracteristic.route';

@NgModule({
  imports: [AuJeuneCampeurSharedModule, RouterModule.forChild(caracteristicRoute)],
  declarations: [CaracteristicComponent, CaracteristicDetailComponent, CaracteristicUpdateComponent, CaracteristicDeleteDialogComponent],
  entryComponents: [CaracteristicDeleteDialogComponent],
})
export class AuJeuneCampeurCaracteristicModule {}
