import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICaracteristic, Caracteristic } from 'app/shared/model/caracteristic.model';
import { CaracteristicService } from './caracteristic.service';
import { CaracteristicComponent } from './caracteristic.component';
import { CaracteristicDetailComponent } from './caracteristic-detail.component';
import { CaracteristicUpdateComponent } from './caracteristic-update.component';

@Injectable({ providedIn: 'root' })
export class CaracteristicResolve implements Resolve<ICaracteristic> {
  constructor(private service: CaracteristicService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaracteristic> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((caracteristic: HttpResponse<Caracteristic>) => {
          if (caracteristic.body) {
            return of(caracteristic.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Caracteristic());
  }
}

export const caracteristicRoute: Routes = [
  {
    path: '',
    component: CaracteristicComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Caracteristics',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaracteristicDetailComponent,
    resolve: {
      caracteristic: CaracteristicResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Caracteristics',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaracteristicUpdateComponent,
    resolve: {
      caracteristic: CaracteristicResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Caracteristics',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaracteristicUpdateComponent,
    resolve: {
      caracteristic: CaracteristicResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'Caracteristics',
    },
    canActivate: [UserRouteAccessService],
  },
];
