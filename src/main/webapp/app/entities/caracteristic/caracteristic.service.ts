import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';

type EntityResponseType = HttpResponse<ICaracteristic>;
type EntityArrayResponseType = HttpResponse<ICaracteristic[]>;

@Injectable({ providedIn: 'root' })
export class CaracteristicService {
  public resourceUrl = SERVER_API_URL + 'api/caracteristics';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/caracteristics';

  constructor(protected http: HttpClient) {}

  create(caracteristic: ICaracteristic): Observable<EntityResponseType> {
    return this.http.post<ICaracteristic>(this.resourceUrl, caracteristic, { observe: 'response' });
  }

  update(caracteristic: ICaracteristic): Observable<EntityResponseType> {
    return this.http.put<ICaracteristic>(this.resourceUrl, caracteristic, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICaracteristic>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICaracteristic[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICaracteristic[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }

  getCaracteristicsOfProduct(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<ICaracteristic[]>(`${this.resourceUrl}OfProduct/${id}`, { observe: 'response' });
  }
}
