import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.AuJeuneCampeurCustomerModule),
      },
      {
        path: 'address',
        loadChildren: () => import('./address/address.module').then(m => m.AuJeuneCampeurAddressModule),
      },
      {
        path: 'manager',
        loadChildren: () => import('./manager/manager.module').then(m => m.AuJeuneCampeurManagerModule),
      },
      {
        path: 'order',
        loadChildren: () => import('./order/order.module').then(m => m.AuJeuneCampeurOrderModule),
      },
      {
        path: 'shopping-cart',
        loadChildren: () => import('./shopping-cart/shopping-cart.module').then(m => m.AuJeuneCampeurShoppingCartModule),
      },
      {
        path: 'order-line',
        loadChildren: () => import('./order-line/order-line.module').then(m => m.AuJeuneCampeurOrderLineModule),
      },
      {
        path: 'product-model',
        loadChildren: () => import('./product-model/product-model.module').then(m => m.AuJeuneCampeurProductModelModule),
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.AuJeuneCampeurProductModule),
      },
      {
        path: 'caracteristic',
        loadChildren: () => import('./caracteristic/caracteristic.module').then(m => m.AuJeuneCampeurCaracteristicModule),
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.AuJeuneCampeurCategoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class AuJeuneCampeurEntityModule {}
