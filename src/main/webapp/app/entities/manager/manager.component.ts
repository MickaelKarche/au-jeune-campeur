import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IManager } from 'app/shared/model/manager.model';
import { ManagerService } from './manager.service';
import { ManagerDeleteDialogComponent } from './manager-delete-dialog.component';

@Component({
  selector: 'jhi-manager',
  templateUrl: './manager.component.html',
})
export class ManagerComponent implements OnInit, OnDestroy {
  managers?: IManager[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected managerService: ManagerService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.managerService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IManager[]>) => (this.managers = res.body || []));
      return;
    }

    this.managerService.query().subscribe((res: HttpResponse<IManager[]>) => (this.managers = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInManagers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IManager): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInManagers(): void {
    this.eventSubscriber = this.eventManager.subscribe('managerListModification', () => this.loadAll());
  }

  delete(manager: IManager): void {
    const modalRef = this.modalService.open(ManagerDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.manager = manager;
  }
}
