import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOrder, Order } from 'app/shared/model/order.model';
import { OrderService } from './order.service';
import { IAddress } from 'app/shared/model/address.model';
import { AddressService } from 'app/entities/address/address.service';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ShoppingCartService } from 'app/entities/shopping-cart/shopping-cart.service';
import { ICustomer } from 'app/shared/model/customer.model';
import { CustomerService } from 'app/entities/customer/customer.service';

type SelectableEntity = IAddress | IShoppingCart | ICustomer;

@Component({
  selector: 'jhi-order-update',
  templateUrl: './order-update.component.html',
})
export class OrderUpdateComponent implements OnInit {
  isSaving = false;
  deliveryaddresses: IAddress[] = [];
  shoppingcarts: IShoppingCart[] = [];
  customers: ICustomer[] = [];
  validationDateDp: any;

  editForm = this.fb.group({
    id: [],
    validationDate: [null, [Validators.required]],
    deliveryLastName: [null, [Validators.required]],
    deliveryFirstName: [null, [Validators.required]],
    status: [],
    deliveryAddress: [],
    shoppingCart: [],
    customer: [],
  });

  constructor(
    protected orderService: OrderService,
    protected addressService: AddressService,
    protected shoppingCartService: ShoppingCartService,
    protected customerService: CustomerService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ order }) => {
      this.updateForm(order);

      this.addressService
        .query({ filter: 'order-is-null' })
        .pipe(
          map((res: HttpResponse<IAddress[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IAddress[]) => {
          if (!order.deliveryAddress || !order.deliveryAddress.id) {
            this.deliveryaddresses = resBody;
          } else {
            this.addressService
              .find(order.deliveryAddress.id)
              .pipe(
                map((subRes: HttpResponse<IAddress>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IAddress[]) => (this.deliveryaddresses = concatRes));
          }
        });

      this.shoppingCartService
        .query({ filter: 'order-is-null' })
        .pipe(
          map((res: HttpResponse<IShoppingCart[]>) => {
            return res.body || [];
          })
        )
        .subscribe((resBody: IShoppingCart[]) => {
          if (!order.shoppingCart || !order.shoppingCart.id) {
            this.shoppingcarts = resBody;
          } else {
            this.shoppingCartService
              .find(order.shoppingCart.id)
              .pipe(
                map((subRes: HttpResponse<IShoppingCart>) => {
                  return subRes.body ? [subRes.body].concat(resBody) : resBody;
                })
              )
              .subscribe((concatRes: IShoppingCart[]) => (this.shoppingcarts = concatRes));
          }
        });

      this.customerService.query().subscribe((res: HttpResponse<ICustomer[]>) => (this.customers = res.body || []));
    });
  }

  updateForm(order: IOrder): void {
    this.editForm.patchValue({
      id: order.id,
      validationDate: order.validationDate,
      deliveryLastName: order.deliveryLastName,
      deliveryFirstName: order.deliveryFirstName,
      status: order.status,
      deliveryAddress: order.deliveryAddress,
      shoppingCart: order.shoppingCart,
      customer: order.customer,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const order = this.createFromForm();
    if (order.id !== undefined) {
      this.subscribeToSaveResponse(this.orderService.update(order));
    } else {
      this.subscribeToSaveResponse(this.orderService.create(order));
    }
  }

  private createFromForm(): IOrder {
    return {
      ...new Order(),
      id: this.editForm.get(['id'])!.value,
      validationDate: this.editForm.get(['validationDate'])!.value,
      deliveryLastName: this.editForm.get(['deliveryLastName'])!.value,
      deliveryFirstName: this.editForm.get(['deliveryFirstName'])!.value,
      status: this.editForm.get(['status'])!.value,
      deliveryAddress: this.editForm.get(['deliveryAddress'])!.value,
      shoppingCart: this.editForm.get(['shoppingCart'])!.value,
      customer: this.editForm.get(['customer'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrder>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }
}
