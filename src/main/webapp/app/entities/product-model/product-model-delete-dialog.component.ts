import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProductModel } from 'app/shared/model/product-model.model';
import { ProductModelService } from './product-model.service';

@Component({
  templateUrl: './product-model-delete-dialog.component.html',
})
export class ProductModelDeleteDialogComponent {
  productModel?: IProductModel;

  constructor(
    protected productModelService: ProductModelService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productModelService.delete(id).subscribe(() => {
      this.eventManager.broadcast('productModelListModification');
      this.activeModal.close();
    });
  }
}
