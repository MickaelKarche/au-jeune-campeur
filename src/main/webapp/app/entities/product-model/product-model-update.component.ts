import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IProductModel, ProductModel } from 'app/shared/model/product-model.model';
import { ProductModelService } from './product-model.service';

@Component({
  selector: 'jhi-product-model-update',
  templateUrl: './product-model-update.component.html',
})
export class ProductModelUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    label: [null, [Validators.required]],
    brand: [null, [Validators.required]],
    price: [null, [Validators.required, Validators.min(0)]],
    description: [null, [Validators.required]],
    imageUrl: [],
    nbSelled: [null, [Validators.min(0)]],
  });

  constructor(protected productModelService: ProductModelService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ productModel }) => {
      this.updateForm(productModel);
    });
  }

  updateForm(productModel: IProductModel): void {
    this.editForm.patchValue({
      id: productModel.id,
      label: productModel.label,
      brand: productModel.brand,
      price: productModel.price,
      description: productModel.description,
      imageUrl: productModel.imageUrl,
      nbSelled: productModel.nbSelled,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const productModel = this.createFromForm();
    if (productModel.id !== undefined) {
      this.subscribeToSaveResponse(this.productModelService.update(productModel));
    } else {
      this.subscribeToSaveResponse(this.productModelService.create(productModel));
    }
  }

  private createFromForm(): IProductModel {
    return {
      ...new ProductModel(),
      id: this.editForm.get(['id'])!.value,
      label: this.editForm.get(['label'])!.value,
      brand: this.editForm.get(['brand'])!.value,
      price: this.editForm.get(['price'])!.value,
      description: this.editForm.get(['description'])!.value,
      imageUrl: this.editForm.get(['imageUrl'])!.value,
      nbSelled: this.editForm.get(['nbSelled'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProductModel>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
