import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductModel } from 'app/shared/model/product-model.model';
import { ProductModelService } from './product-model.service';
import { ProductModelDeleteDialogComponent } from './product-model-delete-dialog.component';

@Component({
  selector: 'jhi-product-model',
  templateUrl: './product-model.component.html',
})
export class ProductModelComponent implements OnInit, OnDestroy {
  productModels?: IProductModel[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected productModelService: ProductModelService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.productModelService
        .search({
          query: this.currentSearch,
        })
        .subscribe((res: HttpResponse<IProductModel[]>) => (this.productModels = res.body || []));
      return;
    }

    this.productModelService.query().subscribe((res: HttpResponse<IProductModel[]>) => (this.productModels = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInProductModels();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IProductModel): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInProductModels(): void {
    this.eventSubscriber = this.eventManager.subscribe('productModelListModification', () => this.loadAll());
  }

  delete(productModel: IProductModel): void {
    const modalRef = this.modalService.open(ProductModelDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.productModel = productModel;
  }
}
