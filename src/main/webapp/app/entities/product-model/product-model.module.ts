import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuJeuneCampeurSharedModule } from 'app/shared/shared.module';
import { ProductModelComponent } from './product-model.component';
import { ProductModelDetailComponent } from './product-model-detail.component';
import { ProductModelUpdateComponent } from './product-model-update.component';
import { ProductModelDeleteDialogComponent } from './product-model-delete-dialog.component';
import { productModelRoute } from './product-model.route';

@NgModule({
  imports: [AuJeuneCampeurSharedModule, RouterModule.forChild(productModelRoute)],
  declarations: [ProductModelComponent, ProductModelDetailComponent, ProductModelUpdateComponent, ProductModelDeleteDialogComponent],
  entryComponents: [ProductModelDeleteDialogComponent],
})
export class AuJeuneCampeurProductModelModule {}
