import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IProductModel, ProductModel } from 'app/shared/model/product-model.model';
import { ProductModelService } from './product-model.service';
import { ProductModelComponent } from './product-model.component';
import { ProductModelDetailComponent } from './product-model-detail.component';
import { ProductModelUpdateComponent } from './product-model-update.component';

@Injectable({ providedIn: 'root' })
export class ProductModelResolve implements Resolve<IProductModel> {
  constructor(private service: ProductModelService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductModel> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((productModel: HttpResponse<ProductModel>) => {
          if (productModel.body) {
            return of(productModel.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ProductModel());
  }
}

export const productModelRoute: Routes = [
  {
    path: '',
    component: ProductModelComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductModels',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductModelDetailComponent,
    resolve: {
      productModel: ProductModelResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductModels',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductModelUpdateComponent,
    resolve: {
      productModel: ProductModelResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductModels',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductModelUpdateComponent,
    resolve: {
      productModel: ProductModelResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'ProductModels',
    },
    canActivate: [UserRouteAccessService],
  },
];
