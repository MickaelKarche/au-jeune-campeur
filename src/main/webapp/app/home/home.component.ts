import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { HttpResponse } from '@angular/common/http';
import { IProductModel } from '../shared/model/product-model.model';
import { ProductModelService } from '../entities/product-model/product-model.service';
import { IOrderLine } from 'app/shared/model/order-line.model';
import { NavbarService } from 'app/customer-interface/service/navbar.service';
import { OrderLineComponentService } from 'app/customer-interface/service/order-line.service';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  listProductModel?: IProductModel[] | null;
  allCategories?: ICategory[] | null;
  childrenCategories?: ICategory[] = [];
  rootCategory: ICategory | undefined;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private navbarComponentService: NavbarService,
    private orderLineComponentService: OrderLineComponentService,
    protected categoryService: CategoryService,
    protected productService: ProductModelService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    if (this.accountService.isAuthenticated()) {
      let c = 0;
      this.orderLineComponentService.getOrderLine().then((data: IOrderLine[]) => {
        data.forEach(element => {
          if (element.quantity) {
            c += element.quantity;
            this.navbarComponentService.nextNbElementShoppingCart(c);
          }
        });
      });
    }
    this.productService.query().subscribe((value: HttpResponse<IProductModel[]>) => {
      this.listProductModel = value.body;

      if (this.listProductModel) {
        this.listProductModel.sort((a, b) => b.nbSelled! - a.nbSelled!);
      }
    });

    this.categoryService.query().subscribe((value: HttpResponse<ICategory[]>) => {
      this.allCategories = value.body;
      this.rootCategory = this.categoryService.getRootCategory(this.allCategories!);
      this.categoryService.getChildrenOfCategory(this.rootCategory.id!).subscribe(value1 => {
        const gotCats = value1.body as ICategory[];
        gotCats.forEach(elem => {
          this.childrenCategories?.push(elem);
        });
        console.log(this.childrenCategories);
      });
    });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}
