import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuJeuneCampeurSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [AuJeuneCampeurSharedModule, RouterModule.forChild(HOME_ROUTE), MatIconModule],
  declarations: [HomeComponent],
  exports: [MatIconModule],
})
export class AuJeuneCampeurHomeModule {}
