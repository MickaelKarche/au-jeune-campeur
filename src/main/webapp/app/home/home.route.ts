import { Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { ListProductComponent } from 'app/customer-interface/list-product/list-product.component';
import { ProductViewComponent } from 'app/customer-interface/product-view/product-view.component';
import { LastPageComponent } from 'app/customer-interface/last-page/last-page.component';

import { ShoppingCartViewComponent } from 'app/customer-interface/shopping-cart-view/shopping-cart-view.component';

import { AdminProductViewComponent } from 'app/admin-interface/admin-product-view/admin-product-view.component';
import { FilterComponent } from 'app/customer-interface/filter/filter.component';
import { OrderPipelineComponent } from 'app/customer-interface/order-pipeline/order-pipeline.component';
import { CategoryComponent } from 'app/customer-interface/category/category.component';
import { AdminStockViewComponent } from 'app/admin-interface/admin-stock-view/admin-stock-view.component';
import { Authority } from 'app/shared/constants/authority.constants';
import { AdminInterfaceAddNewStockComponent } from 'app/admin-interface/admin-interface-add-new-stock/admin-interface-add-new-stock.component';

export const HOME_ROUTE: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: {
      authorities: [],
      pageTitle: 'Welcome, Java Hipster!',
    },
  },
  {
    path: 'list',
    component: ListProductComponent,
    data: {
      authorities: [],
      pageTitle: 'List !',
    },
  },
  {
    path: 'p/:id',
    component: ProductViewComponent,
    data: {
      authorities: [],
      pageTitle: 'List !',
    },
  },
  {
    path: 'c/:name',
    component: CategoryComponent,
    data: {
      authorities: [],
      pageTitle: 'Category !',
    },
  },
  {
    path: 'panier',
    component: ShoppingCartViewComponent,
    data: {
      authorities: [],
      pageTitle: 'Panier !',
    },
  },
  {
    path: 'test',
    component: FilterComponent,
    data: {
      authorities: [],
      pageTitle: 'Category !',
    },
  },
  {
    path: 'adminListProduct',
    component: AdminProductViewComponent,
    data: {
      authorities: [],
      pageTitle: 'adminProductView !',
    },
  },
  {
    path: 'paiement',
    component: OrderPipelineComponent,
    data: {
      authorities: [],
      pageTitle: 'Paiement !',
    },
  },
  {
    path: 'fin',
    component: LastPageComponent,
    data: {
      authorities: [],
      pageTitle: 'Terminé !',
    },
  },
  {
    path: 'pm/stock/:id',
    component: AdminStockViewComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'Stock',
    },
  },
  {
    path: 'pm/stock/:id/new',
    component: AdminInterfaceAddNewStockComponent,
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'Stock',
    },
  },
];
