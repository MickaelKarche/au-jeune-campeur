import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { VERSION } from 'app/app.constants';
import { AccountService } from 'app/core/auth/account.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { LoginService } from 'app/core/login/login.service';
import { ProfileService } from 'app/layouts/profiles/profile.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { HttpResponse } from '@angular/common/http';
import { faShoppingCart, faUser, faTruckLoading, faHiking } from '@fortawesome/free-solid-svg-icons';
import { NavbarService } from '../../customer-interface/service/navbar.service';

@Component({
  selector: 'jhi-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['navbar.scss'],
})
export class NavbarComponent implements OnInit {
  nbElementShoppingCart: number | undefined;
  isCheckingOut: Boolean | undefined;
  inProduction?: boolean;
  isNavbarCollapsed = true;
  swaggerEnabled?: boolean;
  version: string;
  navbarOpen = false;
  listCategory?: ICategory[] | null;
  cartIcon = faShoppingCart;
  userIcon = faUser;
  orderIcon = faTruckLoading;
  activityIcon = faHiking;

  constructor(
    private loginService: LoginService,
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private profileService: ProfileService,
    private navbarService: NavbarService,
    private router: Router,
    private categoryService: CategoryService
  ) {
    this.version = VERSION ? (VERSION.toLowerCase().startsWith('v') ? VERSION : 'v' + VERSION) : '';
  }

  ngOnInit(): void {
    this.navbarService.sharedNumber.subscribe(nbElementShoppingCart => (this.nbElementShoppingCart = nbElementShoppingCart));
    this.navbarService.sharedBoolean.subscribe(boolCheckOut => (this.isCheckingOut = boolCheckOut));
    this.profileService.getProfileInfo().subscribe(profileInfo => {
      this.inProduction = profileInfo.inProduction;
      this.swaggerEnabled = profileInfo.swaggerEnabled;
    });
    this.categoryService.query().subscribe((value: HttpResponse<ICategory[]>) => {
      this.listCategory = value.body;
    });
  }

  toggleNavbarBurger(): void {
    this.navbarOpen = !this.navbarOpen;
  }

  collapseNavbar(): void {
    this.isNavbarCollapsed = true;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  logout(): void {
    this.collapseNavbar();
    this.loginService.logout();
    this.navbarService.nextNbElementShoppingCart(0);
    this.router.navigate(['']);
  }

  toggleNavbar(): void {
    this.isNavbarCollapsed = !this.isNavbarCollapsed;
  }

  getImageUrl(): string {
    return this.isAuthenticated() ? this.accountService.getImageUrl() : '';
  }

  updateCategoryList(): void {
    this.categoryService.query().subscribe((value: HttpResponse<ICategory[]>) => {
      this.listCategory = value.body;
    });
  }
}
