export interface IAddress {
  id?: number;
  number?: string;
  street?: string;
  city?: string;
  postalCode?: string;
}

export class Address implements IAddress {
  constructor(public id?: number, public number?: string, public street?: string, public city?: string, public postalCode?: string) {}
}
