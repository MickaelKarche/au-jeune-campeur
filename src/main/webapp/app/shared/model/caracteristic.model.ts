import { IProduct } from 'app/shared/model/product.model';

export interface ICaracteristic {
  id?: number;
  label?: string;
  value?: string;
  products?: IProduct[];
}

export class Caracteristic implements ICaracteristic {
  constructor(public id?: number, public label?: string, public value?: string, public products?: IProduct[]) {}
}
