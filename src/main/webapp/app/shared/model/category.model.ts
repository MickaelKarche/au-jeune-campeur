import { IProductModel } from 'app/shared/model/product-model.model';

export interface ICategory {
  id?: number;
  name?: string;
  parents?: ICategory[];
  productModels?: IProductModel[];
  category?: ICategory;
}

export class Category implements ICategory {
  constructor(
    public id?: number,
    public name?: string,
    public parents?: ICategory[],
    public productModels?: IProductModel[],
    public category?: ICategory
  ) {}
}
