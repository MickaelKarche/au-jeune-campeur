import { Moment } from 'moment';
import { IAddress } from 'app/shared/model/address.model';
import { IOrder } from 'app/shared/model/order.model';
import { IUser } from '../../core/user/user.model';

export interface ICustomer {
  id?: number;
  lastName?: string;
  firstName?: string;
  passwordHash?: string;
  birthDate?: Moment;
  phoneNumber?: string;
  creditCardHash?: string;
  address?: IAddress;
  orders?: IOrder[];
  user?: IUser;
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public lastName?: string,
    public firstName?: string,
    public passwordHash?: string,
    public birthDate?: Moment,
    public phoneNumber?: string,
    public creditCardHash?: string,
    public address?: IAddress,
    public orders?: IOrder[],
    public user?: IUser
  ) {}
}
