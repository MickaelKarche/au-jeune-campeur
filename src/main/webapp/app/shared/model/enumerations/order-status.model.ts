export const enum OrderStatus {
  COMPLETED = 'COMPLETED',

  PAID = 'PAID',

  SENT = 'SENT',

  INPROCESS = 'INPROCESS',
}
