export interface IManager {
  id?: number;
  lastName?: string;
  firstName?: string;
}

export class Manager implements IManager {
  constructor(public id?: number, public lastName?: string, public firstName?: string) {}
}
