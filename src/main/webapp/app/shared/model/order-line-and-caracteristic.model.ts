import { ICaracteristic } from './caracteristic.model';
import { IOrderLine } from './order-line.model';

export interface IOrderLineAndCaracteristic {
  caracteristics?: ICaracteristic;
  orderline?: IOrderLine;
}

export class OrderLineAndCaracteristic implements IOrderLineAndCaracteristic {
  constructor(public caracteristics?: ICaracteristic, public orderline?: IOrderLine) {}
}
