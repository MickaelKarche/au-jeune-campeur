import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { IProduct } from 'app/shared/model/product.model';

export interface IOrderLine {
  id?: number;
  quantity?: number;
  priceLine?: number;
  shoppingCart?: IShoppingCart;
  product?: IProduct;
}

export class OrderLine implements IOrderLine {
  constructor(
    public id?: number,
    public quantity?: number,
    public priceLine?: number,
    public shoppingCart?: IShoppingCart,
    public product?: IProduct
  ) {}
}
