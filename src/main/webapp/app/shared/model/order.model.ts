import { Moment } from 'moment';
import { IAddress } from 'app/shared/model/address.model';
import { IShoppingCart } from 'app/shared/model/shopping-cart.model';
import { ICustomer } from 'app/shared/model/customer.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';

export interface IOrder {
  id?: any;
  validationDate?: Moment;
  deliveryLastName?: string;
  deliveryFirstName?: string;
  status?: OrderStatus;
  deliveryAddress?: IAddress;
  shoppingCart?: IShoppingCart;
  customer?: ICustomer;
}

export class Order implements IOrder {
  constructor(
    public id?: any,
    public validationDate?: Moment,
    public deliveryLastName?: string,
    public deliveryFirstName?: string,
    public status?: OrderStatus,
    public deliveryAddress?: IAddress,
    public shoppingCart?: IShoppingCart,
    public customer?: ICustomer
  ) {}
}
