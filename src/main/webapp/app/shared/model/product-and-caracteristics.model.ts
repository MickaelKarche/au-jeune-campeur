import { ICaracteristic } from './caracteristic.model';
import { IProduct } from './product.model';

export interface IProductAndCaracteristics {
  caracteristics?: ICaracteristic[];
  product?: IProduct;
}

export class ProductAndCaracteristics implements IProductAndCaracteristics {
  constructor(public product?: IProduct, public caracteristics?: ICaracteristic[]) {}
}
