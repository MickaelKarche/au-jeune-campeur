import { IProduct } from 'app/shared/model/product.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IProductModel {
  id?: number;
  label?: string;
  brand?: string;
  price?: number;
  description?: string;
  imageUrl?: string;
  nbSelled?: number;
  products?: IProduct[];
  categories?: ICategory[];
}

export class ProductModel implements IProductModel {
  constructor(
    public id?: number,
    public label?: string,
    public brand?: string,
    public price?: number,
    public description?: string,
    public imageUrl?: string,
    public nbSelled?: number,
    public products?: IProduct[],
    public categories?: ICategory[]
  ) {}
}
