import { IOrderLine } from 'app/shared/model/order-line.model';
import { IProductModel } from 'app/shared/model/product-model.model';
import { ICaracteristic } from 'app/shared/model/caracteristic.model';

export interface IProduct {
  id?: number;
  nbStock?: number;
  version?: number;
  orderLines?: IOrderLine[];
  productModel?: IProductModel;
  caracteristics?: ICaracteristic[];
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public nbStock?: number,
    public version?: number,
    public orderLines?: IOrderLine[],
    public productModel?: IProductModel,
    public caracteristics?: ICaracteristic[]
  ) {}
}
