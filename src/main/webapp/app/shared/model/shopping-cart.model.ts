import { IOrderLine } from 'app/shared/model/order-line.model';

export interface IShoppingCart {
  id?: number;
  totalPrice?: number;
  orderLines?: IOrderLine[];
}

export class ShoppingCart implements IShoppingCart {
  constructor(public id?: number, public totalPrice?: number, public orderLines?: IOrderLine[]) {}
}
