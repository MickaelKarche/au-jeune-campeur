package com.aujeunecampeur.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.aujeunecampeur.app.web.rest.TestUtil;

public class CaracteristicTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Caracteristic.class);
        Caracteristic caracteristic1 = new Caracteristic();
        caracteristic1.setId(1L);
        Caracteristic caracteristic2 = new Caracteristic();
        caracteristic2.setId(caracteristic1.getId());
        assertThat(caracteristic1).isEqualTo(caracteristic2);
        caracteristic2.setId(2L);
        assertThat(caracteristic1).isNotEqualTo(caracteristic2);
        caracteristic1.setId(null);
        assertThat(caracteristic1).isNotEqualTo(caracteristic2);
    }
}
