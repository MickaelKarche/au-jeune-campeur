package com.aujeunecampeur.app.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.aujeunecampeur.app.web.rest.TestUtil;

public class ProductModelTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductModel.class);
        ProductModel productModel1 = new ProductModel();
        productModel1.setId(1L);
        ProductModel productModel2 = new ProductModel();
        productModel2.setId(productModel1.getId());
        assertThat(productModel1).isEqualTo(productModel2);
        productModel2.setId(2L);
        assertThat(productModel1).isNotEqualTo(productModel2);
        productModel1.setId(null);
        assertThat(productModel1).isNotEqualTo(productModel2);
    }
}
