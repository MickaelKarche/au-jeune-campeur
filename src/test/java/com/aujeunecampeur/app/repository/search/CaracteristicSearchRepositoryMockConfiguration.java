package com.aujeunecampeur.app.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link CaracteristicSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class CaracteristicSearchRepositoryMockConfiguration {

    @MockBean
    private CaracteristicSearchRepository mockCaracteristicSearchRepository;

}
