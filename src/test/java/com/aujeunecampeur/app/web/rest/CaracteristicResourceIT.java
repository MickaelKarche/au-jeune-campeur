package com.aujeunecampeur.app.web.rest;

import com.aujeunecampeur.app.AuJeuneCampeurApp;
import com.aujeunecampeur.app.domain.Caracteristic;
import com.aujeunecampeur.app.repository.CaracteristicRepository;
import com.aujeunecampeur.app.repository.search.CaracteristicSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CaracteristicResource} REST controller.
 */
@SpringBootTest(classes = AuJeuneCampeurApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CaracteristicResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private CaracteristicRepository caracteristicRepository;

    @Mock
    private CaracteristicRepository caracteristicRepositoryMock;

    /**
     * This repository is mocked in the com.aujeunecampeur.app.repository.search test package.
     *
     * @see com.aujeunecampeur.app.repository.search.CaracteristicSearchRepositoryMockConfiguration
     */
    @Autowired
    private CaracteristicSearchRepository mockCaracteristicSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCaracteristicMockMvc;

    private Caracteristic caracteristic;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Caracteristic createEntity(EntityManager em) {
        Caracteristic caracteristic = new Caracteristic()
            .label(DEFAULT_LABEL)
            .value(DEFAULT_VALUE);
        return caracteristic;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Caracteristic createUpdatedEntity(EntityManager em) {
        Caracteristic caracteristic = new Caracteristic()
            .label(UPDATED_LABEL)
            .value(UPDATED_VALUE);
        return caracteristic;
    }

    @BeforeEach
    public void initTest() {
        caracteristic = createEntity(em);
    }

    @Test
    @Transactional
    public void createCaracteristic() throws Exception {
        int databaseSizeBeforeCreate = caracteristicRepository.findAll().size();
        // Create the Caracteristic
        restCaracteristicMockMvc.perform(post("/api/caracteristics")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caracteristic)))
            .andExpect(status().isCreated());

        // Validate the Caracteristic in the database
        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeCreate + 1);
        Caracteristic testCaracteristic = caracteristicList.get(caracteristicList.size() - 1);
        assertThat(testCaracteristic.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testCaracteristic.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the Caracteristic in Elasticsearch
        verify(mockCaracteristicSearchRepository, times(1)).save(testCaracteristic);
    }

    @Test
    @Transactional
    public void createCaracteristicWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = caracteristicRepository.findAll().size();

        // Create the Caracteristic with an existing ID
        caracteristic.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaracteristicMockMvc.perform(post("/api/caracteristics")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caracteristic)))
            .andExpect(status().isBadRequest());

        // Validate the Caracteristic in the database
        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeCreate);

        // Validate the Caracteristic in Elasticsearch
        verify(mockCaracteristicSearchRepository, times(0)).save(caracteristic);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = caracteristicRepository.findAll().size();
        // set the field null
        caracteristic.setLabel(null);

        // Create the Caracteristic, which fails.


        restCaracteristicMockMvc.perform(post("/api/caracteristics")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caracteristic)))
            .andExpect(status().isBadRequest());

        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = caracteristicRepository.findAll().size();
        // set the field null
        caracteristic.setValue(null);

        // Create the Caracteristic, which fails.


        restCaracteristicMockMvc.perform(post("/api/caracteristics")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caracteristic)))
            .andExpect(status().isBadRequest());

        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCaracteristics() throws Exception {
        // Initialize the database
        caracteristicRepository.saveAndFlush(caracteristic);

        // Get all the caracteristicList
        restCaracteristicMockMvc.perform(get("/api/caracteristics?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caracteristic.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllCaracteristicsWithEagerRelationshipsIsEnabled() throws Exception {
        when(caracteristicRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCaracteristicMockMvc.perform(get("/api/caracteristics?eagerload=true"))
            .andExpect(status().isOk());

        verify(caracteristicRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllCaracteristicsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(caracteristicRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCaracteristicMockMvc.perform(get("/api/caracteristics?eagerload=true"))
            .andExpect(status().isOk());

        verify(caracteristicRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getCaracteristic() throws Exception {
        // Initialize the database
        caracteristicRepository.saveAndFlush(caracteristic);

        // Get the caracteristic
        restCaracteristicMockMvc.perform(get("/api/caracteristics/{id}", caracteristic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caracteristic.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE));
    }
    @Test
    @Transactional
    public void getNonExistingCaracteristic() throws Exception {
        // Get the caracteristic
        restCaracteristicMockMvc.perform(get("/api/caracteristics/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCaracteristic() throws Exception {
        // Initialize the database
        caracteristicRepository.saveAndFlush(caracteristic);

        int databaseSizeBeforeUpdate = caracteristicRepository.findAll().size();

        // Update the caracteristic
        Caracteristic updatedCaracteristic = caracteristicRepository.findById(caracteristic.getId()).get();
        // Disconnect from session so that the updates on updatedCaracteristic are not directly saved in db
        em.detach(updatedCaracteristic);
        updatedCaracteristic
            .label(UPDATED_LABEL)
            .value(UPDATED_VALUE);

        restCaracteristicMockMvc.perform(put("/api/caracteristics")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCaracteristic)))
            .andExpect(status().isOk());

        // Validate the Caracteristic in the database
        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeUpdate);
        Caracteristic testCaracteristic = caracteristicList.get(caracteristicList.size() - 1);
        assertThat(testCaracteristic.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testCaracteristic.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the Caracteristic in Elasticsearch
        verify(mockCaracteristicSearchRepository, times(1)).save(testCaracteristic);
    }

    @Test
    @Transactional
    public void updateNonExistingCaracteristic() throws Exception {
        int databaseSizeBeforeUpdate = caracteristicRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaracteristicMockMvc.perform(put("/api/caracteristics")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(caracteristic)))
            .andExpect(status().isBadRequest());

        // Validate the Caracteristic in the database
        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Caracteristic in Elasticsearch
        verify(mockCaracteristicSearchRepository, times(0)).save(caracteristic);
    }

    @Test
    @Transactional
    public void deleteCaracteristic() throws Exception {
        // Initialize the database
        caracteristicRepository.saveAndFlush(caracteristic);

        int databaseSizeBeforeDelete = caracteristicRepository.findAll().size();

        // Delete the caracteristic
        restCaracteristicMockMvc.perform(delete("/api/caracteristics/{id}", caracteristic.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Caracteristic> caracteristicList = caracteristicRepository.findAll();
        assertThat(caracteristicList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Caracteristic in Elasticsearch
        verify(mockCaracteristicSearchRepository, times(1)).deleteById(caracteristic.getId());
    }

    @Test
    @Transactional
    public void searchCaracteristic() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        caracteristicRepository.saveAndFlush(caracteristic);
        when(mockCaracteristicSearchRepository.search(queryStringQuery("id:" + caracteristic.getId())))
            .thenReturn(Collections.singletonList(caracteristic));

        // Search the caracteristic
        restCaracteristicMockMvc.perform(get("/api/_search/caracteristics?query=id:" + caracteristic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caracteristic.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)));
    }
}
