package com.aujeunecampeur.app.web.rest;

import com.aujeunecampeur.app.AuJeuneCampeurApp;
import com.aujeunecampeur.app.domain.ProductModel;
import com.aujeunecampeur.app.repository.ProductModelRepository;
import com.aujeunecampeur.app.repository.search.ProductModelSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductModelResource} REST controller.
 */
@SpringBootTest(classes = AuJeuneCampeurApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductModelResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_BRAND = "AAAAAAAAAA";
    private static final String UPDATED_BRAND = "BBBBBBBBBB";

    private static final Double DEFAULT_PRICE = 0D;
    private static final Double UPDATED_PRICE = 1D;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBBBBBBB";

    private static final Integer DEFAULT_NB_SELLED = 0;
    private static final Integer UPDATED_NB_SELLED = 1;

    @Autowired
    private ProductModelRepository productModelRepository;

    /**
     * This repository is mocked in the com.aujeunecampeur.app.repository.search test package.
     *
     * @see com.aujeunecampeur.app.repository.search.ProductModelSearchRepositoryMockConfiguration
     */
    @Autowired
    private ProductModelSearchRepository mockProductModelSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductModelMockMvc;

    private ProductModel productModel;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductModel createEntity(EntityManager em) {
        ProductModel productModel = new ProductModel()
            .label(DEFAULT_LABEL)
            .brand(DEFAULT_BRAND)
            .price(DEFAULT_PRICE)
            .description(DEFAULT_DESCRIPTION)
            .imageUrl(DEFAULT_IMAGE_URL)
            .nbSelled(DEFAULT_NB_SELLED);
        return productModel;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductModel createUpdatedEntity(EntityManager em) {
        ProductModel productModel = new ProductModel()
            .label(UPDATED_LABEL)
            .brand(UPDATED_BRAND)
            .price(UPDATED_PRICE)
            .description(UPDATED_DESCRIPTION)
            .imageUrl(UPDATED_IMAGE_URL)
            .nbSelled(UPDATED_NB_SELLED);
        return productModel;
    }

    @BeforeEach
    public void initTest() {
        productModel = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductModel() throws Exception {
        int databaseSizeBeforeCreate = productModelRepository.findAll().size();
        // Create the ProductModel
        restProductModelMockMvc.perform(post("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isCreated());

        // Validate the ProductModel in the database
        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeCreate + 1);
        ProductModel testProductModel = productModelList.get(productModelList.size() - 1);
        assertThat(testProductModel.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testProductModel.getBrand()).isEqualTo(DEFAULT_BRAND);
        assertThat(testProductModel.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testProductModel.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProductModel.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testProductModel.getNbSelled()).isEqualTo(DEFAULT_NB_SELLED);

        // Validate the ProductModel in Elasticsearch
        verify(mockProductModelSearchRepository, times(1)).save(testProductModel);
    }

    @Test
    @Transactional
    public void createProductModelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productModelRepository.findAll().size();

        // Create the ProductModel with an existing ID
        productModel.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductModelMockMvc.perform(post("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isBadRequest());

        // Validate the ProductModel in the database
        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeCreate);

        // Validate the ProductModel in Elasticsearch
        verify(mockProductModelSearchRepository, times(0)).save(productModel);
    }


    @Test
    @Transactional
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = productModelRepository.findAll().size();
        // set the field null
        productModel.setLabel(null);

        // Create the ProductModel, which fails.


        restProductModelMockMvc.perform(post("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isBadRequest());

        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBrandIsRequired() throws Exception {
        int databaseSizeBeforeTest = productModelRepository.findAll().size();
        // set the field null
        productModel.setBrand(null);

        // Create the ProductModel, which fails.


        restProductModelMockMvc.perform(post("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isBadRequest());

        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = productModelRepository.findAll().size();
        // set the field null
        productModel.setPrice(null);

        // Create the ProductModel, which fails.


        restProductModelMockMvc.perform(post("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isBadRequest());

        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = productModelRepository.findAll().size();
        // set the field null
        productModel.setDescription(null);

        // Create the ProductModel, which fails.


        restProductModelMockMvc.perform(post("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isBadRequest());

        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProductModels() throws Exception {
        // Initialize the database
        productModelRepository.saveAndFlush(productModel);

        // Get all the productModelList
        restProductModelMockMvc.perform(get("/api/product-models?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)))
            .andExpect(jsonPath("$.[*].nbSelled").value(hasItem(DEFAULT_NB_SELLED)));
    }
    
    @Test
    @Transactional
    public void getProductModel() throws Exception {
        // Initialize the database
        productModelRepository.saveAndFlush(productModel);

        // Get the productModel
        restProductModelMockMvc.perform(get("/api/product-models/{id}", productModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productModel.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.brand").value(DEFAULT_BRAND))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.doubleValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL))
            .andExpect(jsonPath("$.nbSelled").value(DEFAULT_NB_SELLED));
    }
    @Test
    @Transactional
    public void getNonExistingProductModel() throws Exception {
        // Get the productModel
        restProductModelMockMvc.perform(get("/api/product-models/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductModel() throws Exception {
        // Initialize the database
        productModelRepository.saveAndFlush(productModel);

        int databaseSizeBeforeUpdate = productModelRepository.findAll().size();

        // Update the productModel
        ProductModel updatedProductModel = productModelRepository.findById(productModel.getId()).get();
        // Disconnect from session so that the updates on updatedProductModel are not directly saved in db
        em.detach(updatedProductModel);
        updatedProductModel
            .label(UPDATED_LABEL)
            .brand(UPDATED_BRAND)
            .price(UPDATED_PRICE)
            .description(UPDATED_DESCRIPTION)
            .imageUrl(UPDATED_IMAGE_URL)
            .nbSelled(UPDATED_NB_SELLED);

        restProductModelMockMvc.perform(put("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedProductModel)))
            .andExpect(status().isOk());

        // Validate the ProductModel in the database
        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeUpdate);
        ProductModel testProductModel = productModelList.get(productModelList.size() - 1);
        assertThat(testProductModel.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testProductModel.getBrand()).isEqualTo(UPDATED_BRAND);
        assertThat(testProductModel.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testProductModel.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProductModel.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testProductModel.getNbSelled()).isEqualTo(UPDATED_NB_SELLED);

        // Validate the ProductModel in Elasticsearch
        verify(mockProductModelSearchRepository, times(1)).save(testProductModel);
    }

    @Test
    @Transactional
    public void updateNonExistingProductModel() throws Exception {
        int databaseSizeBeforeUpdate = productModelRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductModelMockMvc.perform(put("/api/product-models")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productModel)))
            .andExpect(status().isBadRequest());

        // Validate the ProductModel in the database
        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the ProductModel in Elasticsearch
        verify(mockProductModelSearchRepository, times(0)).save(productModel);
    }

    @Test
    @Transactional
    public void deleteProductModel() throws Exception {
        // Initialize the database
        productModelRepository.saveAndFlush(productModel);

        int databaseSizeBeforeDelete = productModelRepository.findAll().size();

        // Delete the productModel
        restProductModelMockMvc.perform(delete("/api/product-models/{id}", productModel.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductModel> productModelList = productModelRepository.findAll();
        assertThat(productModelList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the ProductModel in Elasticsearch
        verify(mockProductModelSearchRepository, times(1)).deleteById(productModel.getId());
    }

    @Test
    @Transactional
    public void searchProductModel() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        productModelRepository.saveAndFlush(productModel);
        when(mockProductModelSearchRepository.search(queryStringQuery("id:" + productModel.getId())))
            .thenReturn(Collections.singletonList(productModel));

        // Search the productModel
        restProductModelMockMvc.perform(get("/api/_search/product-models?query=id:" + productModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL)))
            .andExpect(jsonPath("$.[*].nbSelled").value(hasItem(DEFAULT_NB_SELLED)));
    }
}
