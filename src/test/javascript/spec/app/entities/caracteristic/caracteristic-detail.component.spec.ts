import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AuJeuneCampeurTestModule } from '../../../test.module';
import { CaracteristicDetailComponent } from 'app/entities/caracteristic/caracteristic-detail.component';
import { Caracteristic } from 'app/shared/model/caracteristic.model';

describe('Component Tests', () => {
  describe('Caracteristic Management Detail Component', () => {
    let comp: CaracteristicDetailComponent;
    let fixture: ComponentFixture<CaracteristicDetailComponent>;
    const route = ({ data: of({ caracteristic: new Caracteristic(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AuJeuneCampeurTestModule],
        declarations: [CaracteristicDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CaracteristicDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CaracteristicDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load caracteristic on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.caracteristic).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
