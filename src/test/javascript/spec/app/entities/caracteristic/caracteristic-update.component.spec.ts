import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { AuJeuneCampeurTestModule } from '../../../test.module';
import { CaracteristicUpdateComponent } from 'app/entities/caracteristic/caracteristic-update.component';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { Caracteristic } from 'app/shared/model/caracteristic.model';

describe('Component Tests', () => {
  describe('Caracteristic Management Update Component', () => {
    let comp: CaracteristicUpdateComponent;
    let fixture: ComponentFixture<CaracteristicUpdateComponent>;
    let service: CaracteristicService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AuJeuneCampeurTestModule],
        declarations: [CaracteristicUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CaracteristicUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaracteristicUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaracteristicService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Caracteristic(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Caracteristic();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
