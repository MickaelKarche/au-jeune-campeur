import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AuJeuneCampeurTestModule } from '../../../test.module';
import { CaracteristicComponent } from 'app/entities/caracteristic/caracteristic.component';
import { CaracteristicService } from 'app/entities/caracteristic/caracteristic.service';
import { Caracteristic } from 'app/shared/model/caracteristic.model';

describe('Component Tests', () => {
  describe('Caracteristic Management Component', () => {
    let comp: CaracteristicComponent;
    let fixture: ComponentFixture<CaracteristicComponent>;
    let service: CaracteristicService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [AuJeuneCampeurTestModule],
        declarations: [CaracteristicComponent],
      })
        .overrideTemplate(CaracteristicComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CaracteristicComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CaracteristicService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Caracteristic(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.caracteristics && comp.caracteristics[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
